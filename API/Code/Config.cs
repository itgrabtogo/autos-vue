﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.WebPages;
using AutosVue.API.Code.ConfigAttributes;

namespace AutosVue.API.Code {
	public static class Config {

		[ManualInitialization]
		public static string ApplicationName { get; set; }
		[ManualInitialization]
		public static string ServerName { get; set; }

		[ManualInitialization]
		public static string MSSQLConnectionString { get; set; }

		[RequiredForApp( "*" )]
		public static string EncryptionKey { get; set; }
		[RequiredForApp( "*" )]
		public static string EncryptionVector { get; set; }


		public static void Init( string AppName, string SrvName ) {
			ApplicationName = AppName;
			ServerName = SrvName;

			// connection strings handled separately
			MSSQLConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings[ "mssql" ]?.ConnectionString ??
									System.Configuration.ConfigurationManager.ConnectionStrings[ "default" ]?.ConnectionString;


			// read settings from web.config
			var properties = typeof( AutosVue.API.Code.Config ).GetProperties( BindingFlags.Public | BindingFlags.Static );
			foreach ( var prop in properties ) {
				if ( prop.GetCustomAttributes( typeof( ManualInitializationAttribute ) ).Any() ) { // skip this one, but check if it's required and has value
					var propValue = prop.GetValue( null );

					// if no value - check if this is required and throw exception
					if ( propValue == null || propValue.ToString().IsEmpty() ) {
						var requiredForApps = prop.GetCustomAttributes( typeof( RequiredForAppAttribute ) );
						foreach ( var attr in requiredForApps ) {
							if ( !( attr is RequiredForAppAttribute ) ) throw new InvalidOperationException();
							if (
								( attr as RequiredForAppAttribute ).Apps.Contains( ApplicationName )
								||
								( attr as RequiredForAppAttribute ).Apps.Contains( "*" )
							)
								throw new ArgumentNullException( prop.Name );
						}
					}

					continue;
				}

				object data = System.Configuration.ConfigurationManager.AppSettings[ prop.Name ];

				// if appSetting with default value not found - check any of the other possible options
				if ( data == null ) {
					var altKeys = prop.GetCustomAttributes( typeof( AltKeyAttribute ) );
					foreach ( var attr in altKeys ) {
						if ( !( attr is AltKeyAttribute ) ) throw new InvalidOperationException();
						data = System.Configuration.ConfigurationManager.ConnectionStrings[ ( attr as AltKeyAttribute ).Key ];
						if ( data != null ) break;
					}
				}

				// if no value - check if this is required and throw exception
				if ( data == null || data.ToString().IsEmpty() ) {
					var requiredForApps = prop.GetCustomAttributes( typeof( RequiredForAppAttribute ) );
					foreach ( var attr in requiredForApps ) {
						if ( !( attr is RequiredForAppAttribute ) ) throw new InvalidOperationException();
						if (
							( attr as RequiredForAppAttribute ).Apps.Contains( ApplicationName )
								||
							( attr as RequiredForAppAttribute ).Apps.Contains( "*" )
						)
							throw new ArgumentNullException( prop.Name );
					}
				}

				// if no value - use the default value
				if ( data == null ) {
					var requiredForApps = prop.GetCustomAttributes( typeof( DefaultValueAttribute ) );
					foreach ( var attr in requiredForApps ) {
						if ( !( attr is DefaultValueAttribute ) ) throw new InvalidOperationException();
						data = ( attr as DefaultValueAttribute )?.Value;
					}
				}

				// set configuration property value
				prop.SetValue( null, Convert.ChangeType( data, prop.PropertyType ) );
			}

		}
	}
}
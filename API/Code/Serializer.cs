﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Web;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace AutosVue.API.Code {

	public enum SerializationType {
		None,
		Binary,
		Protobuf,
		JSON,
		XML
	}

	public static class Serializer {

		public static byte[] Serialize<TEntity>( TEntity x, SerializationType s ) {
			switch ( s ) {
				case SerializationType.Protobuf:
					MemoryStream ms = new MemoryStream();
					ProtoBuf.Serializer.Serialize( ms, x );
					return ms.ToArray();

				case SerializationType.Binary:
					BinaryFormatter ser = new BinaryFormatter();
					MemoryStream bms = new MemoryStream();
					ser.Serialize( bms, x );
					return bms.ToArray();

				case SerializationType.JSON:
					return Encoding.Default.GetBytes( JsonConvert.SerializeObject( x ) );

				case SerializationType.XML:
					XmlSerializer xs = new XmlSerializer( x.GetType() );
					XmlSerializerNamespaces xmlnsEmpty = new XmlSerializerNamespaces();
					xmlnsEmpty.Add( "", "" );
					MemoryStream xms = new MemoryStream();
					xs.Serialize( xms, x, xmlnsEmpty );
					return xms.ToArray();

				case SerializationType.None:
					return null;

				default:
					throw new Exception( "Invalid serialization type" );
			}
		}

		public static TEntity Deserialize<TEntity>( byte[] x, SerializationType s ) {
			switch ( s ) {
				case SerializationType.Protobuf:
					MemoryStream ms = new MemoryStream( x );
					return ProtoBuf.Serializer.Deserialize<TEntity>( ms );

				case SerializationType.Binary:
					BinaryFormatter ser = new BinaryFormatter();
					MemoryStream bms = new MemoryStream( x );
					return (TEntity) ser.Deserialize( bms );

				case SerializationType.JSON:
					return JsonConvert.DeserializeObject<TEntity>( Encoding.Default.GetString( x ) );

				case SerializationType.XML:
					XmlSerializer xser = new XmlSerializer( typeof( TEntity ) );
					MemoryStream xms = new MemoryStream( x );
					return (TEntity) xser.Deserialize( xms );

				case SerializationType.None:
					return default( TEntity );

				default:
					throw new Exception( "Invalid serialization type" );
			}
		}
	}

	public static class Serialization {
		public static TEntity FromXml<TEntity>( this string XML ) where TEntity : new() {
			TEntity res = new TEntity();
			XmlSerializer ser = new XmlSerializer( typeof( TEntity ) );
			StringReader sr = new StringReader( XML );

			try {
				return (TEntity) ser.Deserialize( sr );
			} catch ( Exception err ) {
				throw new Exception( "Unable to deserialize: " + XML, err );
			}
		}

		public static string ToXml( this object o ) {
			XmlSerializer xs = new XmlSerializer( o.GetType() );
			XmlSerializerNamespaces xmlnsEmpty = new XmlSerializerNamespaces();
			xmlnsEmpty.Add( "", "" );
			MemoryStream ms = new MemoryStream();
			xs.Serialize( ms, o, xmlnsEmpty );
			return Encoding.UTF8.GetString( ms.ToArray() );
		}

		public static TEntity FromJson<TEntity>( this string JSON ) where TEntity : new() {
			return JsonConvert.DeserializeObject<TEntity>( JSON, new JsonSerializerSettings() {
				ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
				ObjectCreationHandling = ObjectCreationHandling.Replace,
			} );
		}

		public static string ToJson( this object o ) {
			return JsonConvert.SerializeObject( o, new JsonSerializerSettings() {
				ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
				ObjectCreationHandling = ObjectCreationHandling.Replace,
			} );
		}

		public static TEntity FromProtobuf<TEntity>( this string txt ) where TEntity : new() {
			MemoryStream ms = new MemoryStream( Convert.FromBase64String( txt ) );
			return ProtoBuf.Serializer.Deserialize<TEntity>( ms );
		}

		public static string ToProtobuf( this object o ) {
			MemoryStream ms = new MemoryStream();
			ProtoBuf.Serializer.Serialize( ms, o );
			return Convert.ToBase64String( ms.ToArray() );
		}
	}
}
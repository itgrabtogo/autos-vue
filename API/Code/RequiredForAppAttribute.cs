﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutosVue.API.Code.ConfigAttributes {
	[AttributeUsage( AttributeTargets.Property )]
	public class RequiredForAppAttribute: Attribute {

		public string[] Apps { get; set; }

		public RequiredForAppAttribute( params string[] apps ) {
			Apps = apps;
		}

	}
}

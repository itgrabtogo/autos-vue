﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Dapper;
using Dapper.Contrib.Extensions;
using AutosVue.Models;
using AutosVue.Models.IAAI;
using Newtonsoft.Json;

namespace AutosVue.API.Code {
	public class IAAIparser : PageParser {

		private static Dictionary<string, string> LoginInfo = new Dictionary<string, string>() {
				{ "user", "lubo.petrov@live.com" },
				{ "password", "L0k0m0tiv!" },
				{ "remMe", "true" },
				{ "queryString", "ReturnUrl=/MyVehicles" },
				{ "source", "/Login/LoginPage?ReturnUrl=/MyVehicles" }
			};

		private string Login( string ReturnURL ) {
			return POST( "https://www.iaai.com/Login/LoginPageSubmit/?ReturnUrl=" + ReturnURL, LoginInfo );
		}

		public string ParseCar( IDbConnection db, ref AuctionOffer ThisCar, bool doLogin = false ) {
			if ( doLogin ) {
				Login( "/MyVehicles" );
			}

			var PageData = GET( ThisCar.ItemLink );
			var JSON = ParseNextSection( PageData, "<script type=\"application/json\" id=\"ProductDetailsVM\">", "</script>" );
			var ThisVehicle = JsonConvert.DeserializeObject<AutosVue.Models.IAAI.VehicleDetails>( JSON );

			ThisCar.Title = ParseNextSection( PageData, "<h1 class=\"heading-2 heading-2-semi mb-0\">", "</h1>" );

			ThisCar.AuctionID = ThisVehicle.VehicleDetailsViewModel.AuctionID;
			ThisCar.Make = ThisVehicle.VehicleDetailsViewModel.Make;
			ThisCar.Model = ThisVehicle.VehicleDetailsViewModel.Model;
			ThisCar.Year = ThisVehicle.VehicleDetailsViewModel.Year;
			ThisCar.CountryOfOrigin = MapDisplayInfo( ThisVehicle.VehicleDetailsViewModel.VehicleDescription, "ManufacturedIn" );
			ThisCar.Transmission = MapDisplayInfo( ThisVehicle.VehicleDetailsViewModel.VehicleDescription, "Transmission" );
			ThisCar.DriveLineType = MapDisplayInfo( ThisVehicle.VehicleDetailsViewModel.VehicleDescription, "DriveLineType" );
			ThisCar.Fuel = MapDisplayInfo( ThisVehicle.VehicleDetailsViewModel.VehicleDescription, "FuelType" );
			ThisCar.Engine = MapDisplayInfo( ThisVehicle.VehicleDetailsViewModel.VehicleDescription, "Engine" );
			ThisCar.SellingBranch = ThisVehicle.VehicleDetailsViewModel.BranchLink;

			ThisCar.Odometer = MapDisplayInfo( ThisVehicle.VehicleDetailsViewModel.ConditionInfo, "Odometer" );
			ThisCar.StartCode = MapDisplayInfo( ThisVehicle.VehicleDetailsViewModel.ConditionInfo, "runAndDrive" );

			ThisCar.Loss = MapDisplayInfo( ThisVehicle.VehicleDetailsViewModel.ConditionInfo, "Loss_Origin" );
			ThisCar.PrimaryDamage = MapDisplayInfo( ThisVehicle.VehicleDetailsViewModel.VehicleInformation, "PrimaryDamage" );
			ThisCar.SecondaryDamage = MapDisplayInfo( ThisVehicle.VehicleDetailsViewModel.VehicleInformation, "SecondaryDamage" );
			ThisCar.IsAirbagDeployed = MapDisplayInfo( ThisVehicle.VehicleDetailsViewModel.ConditionInfo, "AirBagDeploymentCheck", "AirBag" ).ToLower().Contains( "deployed" );
			ThisCar.IsKeyPresent = MapDisplayInfo( ThisVehicle.VehicleDetailsViewModel.ConditionOverViewAttributesInfo, "Keys" ).ToLower().Contains( "present" );
			ThisCar.Seller = ThisVehicle.VehicleDetailsViewModel.Seller;
			// ThisCar.VehicleTypeID = ThisVehicle.VehicleDetailsViewModel.VehicleClass; ?? mapping
			ThisCar.VehicleTypeID = MapVehicleBodyStyle( db, MapDisplayInfo( ThisVehicle.VehicleDetailsViewModel.VINInfo, "BodyStyle" ) );
			ThisCar.AuctionLiveDate = ToBGTime( db,
				DateTime.Parse( ThisVehicle.VehicleDetailsViewModel.LiveDateinUserTimeZone, System.Globalization.CultureInfo.GetCultureInfo( "en-US" ) ),
				ThisVehicle.VehicleDetailsViewModel.UserTimezoneAbb );

			ThisCar.CurrentBidAmount = Convert.ToDecimal( ThisVehicle.VehicleDetailsViewModel.DecimalHighBidAmount );
			ThisCar.MinimumBidAmount = Convert.ToDecimal( ThisVehicle.VehicleDetailsViewModel.MinimumBidAmount );
			ThisCar.BuyItAmount = Convert.ToDecimal( ThisVehicle.VehicleDetailsViewModel.BuyNowAmount );

			ThisCar.CompanyID = 1;

			if ( ThisCar.ID == 0 ) {
				if ( ThisCar.AuctionID == 0 ) return "";
				ThisCar.ID = db.Insert( ThisCar );
			} else {
				db.Update( ThisCar );
			}

			db.Execute( @"
					insert into OfferVersions(OfferID, JSON) values (@ID, @JSON)
					
					delete from TransportProposals where OfferID=@ID;
					insert into TransportProposals(OfferID, DeparturePortID, DestinationPortID, CostToDeparturePort, CostToFinalDestination, OceanFreight)
						select top 5 * 
						from ( 
								select distinct
									@ID as OfferID,
									us.PortID as DeparturePortID,
									eu.PortID as DestinationPortID,
									us.TransportCost as CostToDeparturePort,
									eu.TransportCost as CostToFinalDestination,
									ocean.Cost as OceanFreight
								from
									TransportMatrix us,
									TransportMatrix eu,
									PortMatrix ocean
								where
									us.LocationID = @LocationID and
									eu.LocationID = -999 and
									us.PortID = ocean.DeparturePortID and us.UnitsCount = 1 and
									eu.PortID = ocean.DestinationPortID and eu.UnitsCount = 1 and
									ocean.UnitsPerContainer = 1 and
									ocean.Cost is not null
						
							) as query 
						order by CostToDeparturePort + CostToFinalDestination + OceanFreight asc
				", new {
				ThisCar.ID,
				ThisCar.LocationID,
				JSON
			} );

			return JsonConvert.SerializeObject( ThisCar );
		}

		public string ParseMyVehicles( IDbConnection db ) {
			Login( "/MyVehicles" );
			var PageData = POST( "https://www.iaai.com/MyVehicles/ItemsPerPageFilter", new Dictionary<string, string>() {
					{ "SelectedAuctionDate", "" },
					{ "ShowMyStockOnly", "false" },
					{ "IsVisiblePBFilter", "true" },
					{ "SelectedMyActivity", "All" },
					{ "SelectedWhoCanBid", "" },
					{ "SelectedListType", "All" },
					{ "SelectedLocationFilter", "All" },
					{ "FavSearchStockNumber", "" },
					{ "FavSearchVin", "" },
					{ "CurrentPage", "1" },
					{ "PageSize", " 500" },
					{ "SortColumn", "auctiondate" },
					{ "SortAscending", "true" },
					{ "IsExportToExcel", "false" },
					{ "PBFilterSelectedValue", "All" },
				} );

			var result = new List<AuctionOffer>();
			int si = 0;
			while ( true ) {
				// get next car on page
				var CarInfo = ParseNextSection( PageData, "<div class=\"list-row preSaleScan\">", "<input type=\"hidden\" id=\"hdnmyMaxtext_", ref si );
				if ( CarInfo == null ) break;

				// find ItemID from this page
				var CarItemID = int.Parse( ParseTagAttribute( CarInfo, "<input type=\"hidden\" id=\"hdnItemID_", "/>", "value" ) );

				// get car info from database (if previously parsed)
				var ThisCar = db.QuerySingleOrDefault<AuctionOffer>( "select * from AuctionOffers where ItemID=@CarItemID", new { CarItemID } ) ?? new AuctionOffer();

				// if ( CarInfo.Contains( "<li>Vehicle Complete</li>" ) ) ThisCar.AuctionStatusID = 1;

				ThisCar.VIN = ParseNextSection( CarInfo, "<li class=\"vin\">VIN: ", "</li>", false ); // VIN info encrypted in JSON 
				ThisCar.ItemID = CarItemID;
				ThisCar.LocationID = int.Parse( ParseTagAttribute( CarInfo, "<input type=\"hidden\" id=\"hdnBranchCode_", "/>", "value" ) ); // get branch code
				ThisCar.ItemLink = $"https://www.iaai.com/VehicleDetails?itemid={ThisCar.ItemID}";
				ThisCar.AuctionStatusID = ThisCar.AuctionID == 0 ? 1 : ThisCar.AuctionLiveDate != null ? 2 : 3;
				ParseCar( db, ref ThisCar ); // parse all other car info from VehicleDetails page

				if ( ThisCar.ID > 0 ) { // insert if car saved
					result.Add( ThisCar );
				}
			}

			// remove expired auctions
			db.Execute( @"
					update AuctionOffers set AuctionStatusID=1, Active=0 where isnull(IsSelected,0)=0 and isnull(AmountPaid,0)=0 and ID not in @list
					update AuctionOffers set Active=1 where ID in @list
				", new { list = result.Select( x => x.ID ) } );

			return JsonConvert.SerializeObject( result );
		}

		#region mapping
		private static Dictionary<string, int> m_VehicleTypes = new Dictionary<string, int>();
		private static int MapVehicleBodyStyle( IDbConnection db, string BodyStyle ) {
			lock ( m_VehicleTypes ) {
				if ( m_VehicleTypes.Count == 0 ) {
					db.Query<VehicleType>( "select * from VehicleTypes" ).ToList().ForEach( x => m_VehicleTypes.Add( x.Type.Trim().ToLower(), x.ID ) );
				}

				if ( m_VehicleTypes.ContainsKey( BodyStyle.Trim().ToLower() ) ) {
					return m_VehicleTypes[ BodyStyle.Trim().ToLower() ];
				}

				var id = db.QuerySingle<int>( "insert into VehicleTypes (Type) values (@BodyStyle); select scope_identity();", new { BodyStyle } );
				m_VehicleTypes.Add( BodyStyle.Trim().ToLower(), id );

				return id;
			}
		}

		private static string MapDisplayInfo( List<VehicleDisplayInfo> lst, params string[] LookupText ) {
			return string.Join( ", ",
							lst.FirstOrDefault( x => LookupText.Any( s => x.Name.ToLower().Trim().Contains( s.ToLower() ) ) )?
								.DisplayValues?
								.Select( x => x.Text )
								.Distinct()
							?? new string[] { }
						);
		}

		private static Dictionary<string, string> m_TimeZoneMap = null;

		public static string MapTimeZone( IDbConnection db, string timeZoneAbbreviation ) {
			if ( m_TimeZoneMap == null ) {
				m_TimeZoneMap = new Dictionary<string, string>();
				db.Query<AutosVue.Models.TimeZoneInfo>( "select * from TimeZones" ).ToList().ForEach( x => {
					if ( !m_TimeZoneMap.ContainsKey( x.Daylight.Trim().ToLower() ) )
						m_TimeZoneMap.Add( x.Daylight.Trim().ToLower(), x.IANATimeZone );
					if ( !m_TimeZoneMap.ContainsKey( x.Standard.Trim().ToLower() ) )
						m_TimeZoneMap.Add( x.Standard.Trim().ToLower(), x.IANATimeZone );
				} );
			}

			return m_TimeZoneMap[ timeZoneAbbreviation.ToLower().Trim() ];
		}

		public static DateTime ToBGTime( IDbConnection db, DateTime d, string timeZoneAbbreviation ) {
			return d.ConvertToUtc( MapTimeZone( db, timeZoneAbbreviation ) ).ConvertToUtc( "Europe/Sofia" );
		}

		#endregion
	}
}
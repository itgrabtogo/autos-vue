﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AutosVue.API.Code {
	public static class DB {
		public static IDbConnection CreateMSSQL() {
			return new SqlConnection( Config.MSSQLConnectionString );
		}
	}
}
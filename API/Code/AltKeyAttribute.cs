﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutosVue.API.Code.ConfigAttributes {
	[AttributeUsage( AttributeTargets.Property )]
	public class AltKeyAttribute : Attribute {

		public string Key { get; set; }

		public AltKeyAttribute( string key ) {
			Key = key;
		}

	}
}

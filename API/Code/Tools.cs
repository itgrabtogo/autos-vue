﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.WebControls;
using NodaTime;
using NodaTime.TimeZones;

namespace AutosVue.API.Code {

	public static class Tools {

		#region Retrieve Page

		// *********************************************************************************
		//    Download a page
		// *********************************************************************************

		public static string RetrievePage( string PageURL, string PostValue, string username, string password, CookieContainer cookieContainer, ref string ResponseURL, int c_Page_Timeout, string Referrer ) {
			var receiveStream = RetrievePageStream( PageURL, PostValue, username, password, cookieContainer, ref ResponseURL, c_Page_Timeout, Referrer );
			return ReadResponseStream( receiveStream, System.Text.Encoding.GetEncoding( "utf-8" ) );
		}

		public static string RetrievePage( string PageURL, string PostValue, string username, string password, CookieContainer cookieContainer, Encoding encoding, ref string ResponseURL, int c_Page_Timeout, string Referrer ) {
			var receiveStream = RetrievePageStream( PageURL, PostValue, username, password, cookieContainer, ref ResponseURL, c_Page_Timeout, Referrer );

			return ReadResponseStream( receiveStream, encoding );
		}

		public static Stream RetrievePageStream( string PageURL, string PostValue, string username, string password, CookieContainer cookieContainer, ref string ResponseURL, int c_Page_Timeout, string UserAgent = "", string Referrer = "" ) {
			var result = RetrievePageResponse( PageURL, PostValue, username, password, cookieContainer, c_Page_Timeout, UserAgent, Referrer );
			ResponseURL = result.ResponseUri.ToString();
			return result.GetResponseStream();
		}

		public static string ReadResponseStream( Stream ReceiveStream ) {
			return ReadResponseStream( ReceiveStream, System.Text.Encoding.GetEncoding( "utf-8" ) );
		}

		public static string ReadResponseStream( Stream ReceiveStream, Encoding encoding ) {
			using ( StreamReader readStream = new StreamReader( ReceiveStream, encoding ) ) {
				return readStream.ReadToEnd();
			}
		}

		public static WebResponse RetrievePageResponse( string PageURL, string PostValue, string username, string password, CookieContainer cookieContainer, int c_Page_Timeout, string UserAgent = "", string Referrer = "" ) {
			HttpWebRequest Req = (HttpWebRequest) HttpWebRequest.Create( new Uri( PageURL ) );

			if ( cookieContainer != null ) {
				( Req as HttpWebRequest ).CookieContainer = cookieContainer;
			}
			if ( username.NotEmpty() ) {
				NetworkCredential networkCredential = new NetworkCredential( username, password );
				Req.Credentials = networkCredential;
			}
			Req.Timeout = c_Page_Timeout;
			Req.KeepAlive = false;
			Req.Accept = "*/*";
			Req.Headers.Set( "Accept-Language", "en-US,en;q=0.9,bg;q=0.8" );
			Req.Headers.Set( "Cache-Control", "no-cache" );
			Req.KeepAlive = true;
			// Req.Headers.Set( "Accept-Encoding", "gzip, deflate, br" );
			// Req.Headers.Set( "Accept-Encoding", "utf-8" );
			Req.Referer = Referrer;
			if ( UserAgent.IsEmpty() )
				Req.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36";
			else
				Req.UserAgent = UserAgent;
			// Req.Headers.Add( "UA-CPU", "x86" );

			Stream RequestStream;

			byte[] BytesWrite;

			if ( PostValue != "" ) {
				BytesWrite = System.Text.Encoding.UTF8.GetBytes( PostValue );
				Req.Method = "POST";// "GET";
				Req.ContentType = "application/x-www-form-urlencoded";
				Req.ContentLength = BytesWrite.Length;
				RequestStream = Req.GetRequestStream();
				RequestStream.Write( BytesWrite, 0, BytesWrite.Length );
				RequestStream.Close();
			}
			return Req.GetResponse();
		}


		public static string RetrievePage( string PageURL, string PostValue, ref string ResponseURL, int c_Page_Timeout, string Referrer = "" ) {
			return ( RetrievePage( PageURL, PostValue, "", "", null, ref ResponseURL, c_Page_Timeout, Referrer ) );
		}

		public static string RetrievePage( string PageURL, string PostValue, Encoding encoding, ref string ResponseURL, int c_Page_Timeout, string Referrer = "" ) {
			return ( RetrievePage( PageURL, PostValue, "", "", null, encoding, ref ResponseURL, c_Page_Timeout, Referrer ) );
		}

		#endregion

		#region IsEmpty/NotEmpty
		public static bool IsEmpty( this string s, params string[] otherEmptyValues ) {
			return string.IsNullOrWhiteSpace( s ) || otherEmptyValues.Any( x => x == s );
		}

		public static bool NotEmpty( this string s ) {
			return !string.IsNullOrWhiteSpace( s );
		}

		public static bool IsEmpty( this decimal x ) {
			return x > -0.00001M && x < 0.00001M;
		}

		public static bool NotEmpty( this decimal x ) {
			return x < -0.00001M || x > 0.00001M;
		}

		public static bool IsEmpty( this int x ) {
			return x == 0;
		}

		public static bool NotEmpty( this int x ) {
			return x != 0;
		}

		public static bool IsEmpty( this int? x ) {
			return x == null || x.Value == 0;
		}

		public static bool NotEmpty( this int? x ) {
			return x != null && x.Value != 0;
		}
		public static bool IsEmpty( this decimal? x ) {
			return x == null || ( x.Value > -0.00001M && x.Value < 0.00001M );
		}

		public static bool NotEmpty( this decimal? x ) {
			return x != null && ( x.Value < -0.00001M || x.Value > 0.00001M );
		}


		public static bool IsEmpty<T, E>( this T x ) where T : IEnumerable<E> {
			return !NotEmpty<T, E>( x );
		}

		public static bool NotEmpty<T, E>( this T x ) where T : IEnumerable<E> {
			return x != null && !x.Any();
		}


		public static bool IsEmpty<T>( this T x ) where T : class {
			return x == null;
		}

		public static bool IsEmpty( this Guid x ) {
			return x == null || x == Guid.Empty;
		}

		public static bool IsEmpty( this Guid? x ) {
			return !x.HasValue || x == Guid.Empty;
		}
		public static bool NotEmpty<T>( this T x ) where T : class {
			return x != null;
		}
		#endregion

		#region In/NotIn extensions
		public static bool In( this Enum value, params Enum[] values ) {
			return values.Contains( value );
		}

		public static bool In( this string value, params string[] values ) {
			return values.Contains( value );
		}

		public static bool In( this int value, params int[] values ) {
			return values.Contains( value );
		}

		public static bool In( this decimal value, params decimal[] values ) {
			return values.Contains( value );
		}

		public static bool In( this DateTime value, params DateTime[] values ) {
			return values.Contains( value );
		}


		public static bool NotIn( this Enum value, params Enum[] values ) {
			return !values.Contains( value );
		}

		public static bool NotIn( this string value, params string[] values ) {
			return !values.Contains( value );
		}

		public static bool NotIn( this int value, params int[] values ) {
			return !values.Contains( value );
		}

		public static bool NotIn( this decimal value, params decimal[] values ) {
			return !values.Contains( value );
		}

		public static bool NotIn( this DateTime value, params DateTime[] values ) {
			return !values.Contains( value );
		}
		#endregion

		#region datagrid control helpers
		public static TextBox txt( this DataGridItem itm, string name ) {
			return itm.FindControl( name ) as TextBox;
		}

		public static Label lbl( this DataGridItem itm, string name ) {
			return itm.FindControl( name ) as Label;
		}
		public static Literal ltr( this DataGridItem itm, string name ) {
			return itm.FindControl( name ) as Literal;
		}

		public static DropDownList ddl( this DataGridItem itm, string name ) {
			return itm.FindControl( name ) as DropDownList;
		}

		public static CheckBox chk( this DataGridItem itm, string name ) {
			return itm.FindControl( name ) as CheckBox;
		}
		public static Button btn( this DataGridItem itm, string name ) {
			return itm.FindControl( name ) as Button;
		}

		public static LinkButton lnkbtn( this DataGridItem itm, string name ) {
			return itm.FindControl( name ) as LinkButton;
		}

		public static PlaceHolder pnl( this DataGridItem itm, string name ) {
			return itm.FindControl( name ) as PlaceHolder;
		}

		public static Repeater rpt( this DataGridItem itm, string name ) {
			return itm.FindControl( name ) as Repeater;
		}

		public static DataGrid grid( this DataGridItem itm, string name ) {
			return itm.FindControl( name ) as DataGrid;
		}
		public static DataGridItem ParentGridItem( this object sender ) {
			return ( ( sender as TextBox ).Parent.Parent as DataGridItem );
		}
		#endregion

		#region repeater control helpers
		public static TextBox txt( this RepeaterItem itm, string name ) {
			return itm.FindControl( name ) as TextBox;
		}

		public static Label lbl( this RepeaterItem itm, string name ) {
			return itm.FindControl( name ) as Label;
		}

		public static Literal ltr( this RepeaterItem itm, string name ) {
			return itm.FindControl( name ) as Literal;
		}

		public static DropDownList ddl( this RepeaterItem itm, string name ) {
			return itm.FindControl( name ) as DropDownList;
		}

		public static CheckBox chk( this RepeaterItem itm, string name ) {
			return itm.FindControl( name ) as CheckBox;
		}
		public static Button btn( this RepeaterItem itm, string name ) {
			return itm.FindControl( name ) as Button;
		}

		public static LinkButton lnkbtn( this RepeaterItem itm, string name ) {
			return itm.FindControl( name ) as LinkButton;
		}

		public static PlaceHolder pnl( this RepeaterItem itm, string name ) {
			return itm.FindControl( name ) as PlaceHolder;
		}

		public static RepeaterItem ParentRepeaterItem( this object sender ) {
			return ( ( sender as TextBox ).Parent.Parent as RepeaterItem );
		}

		public static Repeater rpt( this RepeaterItem itm, string name ) {
			return itm.FindControl( name ) as Repeater;
		}

		public static DataGrid grid( this RepeaterItem itm, string name ) {
			return itm.FindControl( name ) as DataGrid;
		}
		#endregion

		#region number parsing and data type conversion
		public static bool ToBool( this string s, bool defaultValue = false ) {
			return s.IsEmpty() ? defaultValue : s.ToLower().In( "true", "yes", "1" );
		}

		public static int? ToNullableInt( this string x ) {
			if ( x.IsEmpty() ) return null;
			return int.Parse( x );
		}

		public static int ToInt( this string x ) {
			if ( x.IsEmpty() ) return 0;
			return int.Parse( x );
		}

		public static string DecimalNumbersOnly( this string s ) {
			return Regex.Replace( s, "[^0-9.]", "" );
		}

		public static string IntegerNumbersOnly( this string s ) {
			return Regex.Replace( s, "[^0-9]", "" );
		}

		public static decimal ToDecimal( this string s ) {
			var val = s.DecimalNumbersOnly();
			if ( val.IsEmpty() ) return 0;
			return decimal.Parse( val );
		}

		public static decimal ToInteger( this string s ) {
			return decimal.Parse( s.IntegerNumbersOnly() );
		}
		#endregion

		#region DateTime helper functions
		public static DateTime ConvertToUtc( this DateTime dateTime, string fromTimeZone ) {

			NodaTime.DateTimeZone tz = NodaTime.DateTimeZoneProviders.Tzdb.GetZoneOrNull( fromTimeZone );
			ZoneLocalMappingResolver res = Resolvers.CreateMappingResolver( Resolvers.ReturnLater, Resolvers.ReturnStartOfIntervalAfter );

			return tz.ResolveLocal( LocalDateTime.FromDateTime( dateTime ), res ).ToDateTimeUtc();
		}

		public static DateTime ConvertFromUtc( DateTime dateTime, string toTimeZone ) {
			if ( dateTime.Kind == DateTimeKind.Unspecified )
				dateTime = DateTime.SpecifyKind( dateTime, DateTimeKind.Utc );
			Instant instant = Instant.FromDateTimeUtc( dateTime );
			IDateTimeZoneProvider timeZoneProvider = DateTimeZoneProviders.Tzdb;
			DateTimeZone usersTimezone = timeZoneProvider[ toTimeZone ];
			ZonedDateTime usersZonedDateTime = instant.InZone( usersTimezone );
			return usersZonedDateTime.ToDateTimeUnspecified();
		}

		#endregion
	}
}
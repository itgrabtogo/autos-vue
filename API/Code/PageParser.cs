﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web;

namespace AutosVue.API.Code {

	public class PageParser {

		public const int PAGE_TIMEOUT = 125000;
		public const int MAX_RETRIES = 5;

		public static long NumRequests = 0;
		public static long NumWebErrors = 0;

		public CookieContainer cookies = new CookieContainer();

		public string ResponseURL = "";

		public string ParseNextSection( string page, string sectionStart, string sectionEnd, ref int startIndex, bool includeWrapperTags = false ) {
			startIndex = page.IndexOf( sectionStart, startIndex );
			if ( startIndex == -1 ) return null;
			var endIndex = page.IndexOf( sectionEnd, startIndex );
			if ( endIndex == -1 ) return null;

			var contentStart = startIndex + ( includeWrapperTags ? 0 : sectionStart.Length );

			var contentLength = endIndex - startIndex + ( includeWrapperTags ? sectionEnd.Length : -sectionStart.Length );

			var result = page.Substring( contentStart, contentLength );
			startIndex = endIndex + 1;

			return result;
		}

		public string ParseNextSection( string page, string sectionStart, string sectionEnd, bool includeWrapperTags = false ) {
			int si = 0;
			return ParseNextSection( page, sectionStart, sectionEnd, ref si, includeWrapperTags );
		}

		public string ParseTagAttribute( string section, string tagStart, string tagEnd, string attributeName = null ) {
			var startIndex = section.IndexOf( tagStart );
			if ( startIndex == -1 ) return null;
			var endIndex = section.IndexOf( tagEnd, startIndex );
			if ( endIndex == -1 ) return null;

			if ( attributeName.IsEmpty() ) {
				return section.Substring( startIndex + tagStart.Length, endIndex - startIndex - tagStart.Length );
			} else {
				var attrStart = section.IndexOf( attributeName + "=", startIndex );
				if ( attrStart == -1 ) return null;
				var openingQuoteIndex = section.IndexOfAny( new char[] { '\'', '"' }, attrStart + attributeName.Length );
				if ( openingQuoteIndex == -1 ) return null;
				var quote = section[ openingQuoteIndex ];
				var closingQuoteIndex = openingQuoteIndex + 1;
				while ( closingQuoteIndex < section.Length ) {
					closingQuoteIndex = section.IndexOf( quote, closingQuoteIndex + 1 );
					if ( closingQuoteIndex == -1 ) return null;
					if ( section[ closingQuoteIndex - 1 ] != '\\' ) break;
					closingQuoteIndex++;
				}

				return section.Substring( openingQuoteIndex + 1, closingQuoteIndex - openingQuoteIndex - 1 );
			}
		}

		// --------------------------------------------------------------------------

		public string GET( string URL ) {
			for ( int attempt = 0; attempt < MAX_RETRIES; attempt++ ) {
				try {
					Interlocked.Increment( ref NumRequests );
					return Tools.RetrievePage( URL, "", null, null, cookies, ref ResponseURL, PAGE_TIMEOUT, Referrer: ResponseURL );
				} catch ( WebException ) {
					Interlocked.Increment( ref NumWebErrors );
					if ( attempt < MAX_RETRIES - 1 ) {
						Thread.Sleep( 100 + new Random().Next( 100 ) );
					} else {
						throw;
					}
				}
			}

			throw new Exception( "HTTP GET failed" );
		}

		public string POST( string URL, Dictionary<string, string> Data ) {
			var PostValue = string.Join( "&", Data.Select( x => System.Uri.EscapeDataString( x.Key ) + "=" + System.Uri.EscapeDataString( x.Value ) ) );
			for ( int attempt = 0; attempt < MAX_RETRIES; attempt++ ) {
				try {
					Interlocked.Increment( ref NumRequests );
					return Tools.RetrievePage( URL, PostValue, null, null, cookies, ref ResponseURL, PAGE_TIMEOUT, Referrer: ResponseURL );
				} catch ( WebException ) {
					Interlocked.Increment( ref NumWebErrors );
					if ( attempt < MAX_RETRIES - 1 ) {
						Thread.Sleep( 100 + new Random().Next( 100 ) );
					} else {
						throw;
					}
				}
			}

			throw new Exception( "HTTP POST failed" );
		}

		public void PrintDictionary( Dictionary<string, string> dict ) {
			foreach ( var key in dict.Keys ) {
				Console.WriteLine( key + " = " + dict[ key ] );
			}
		}

	}
}
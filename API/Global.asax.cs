﻿using System;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Http.Validation;
using System.Threading;
using AutosVue.API.Code;

namespace AutosVue.API {
    public class WebApiApplication : System.Web.HttpApplication {


        protected void Application_Start() {
            Config.Init( "Models", Server.MachineName );
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure( WebApiConfig.Register );
            FilterConfig.RegisterGlobalFilters( GlobalFilters.Filters );
            RouteConfig.RegisterRoutes( RouteTable.Routes );
            BundleConfig.RegisterBundles( BundleTable.Bundles );
        }

        protected void Application_End() {
        }

		protected void Application_Error( object sender, EventArgs e ) {
		}
	}
}
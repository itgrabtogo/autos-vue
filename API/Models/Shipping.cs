﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutosVue.Models {
    
    public class ContainerShipping {
        public int DeparturePortID { get; set; }
        public int DestinationPortID { get; set; }
        public int UnitsPerContainer { get; set; }
        public decimal Cost { get; set; }
    }
    public class Port {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
    }
    public class TransportMatrix {
        public int AuctionID { get; set; }
        public int PortID { get; set; }
        public decimal TransportCost { get; set; }
        public int UnitsCount { get; set; }
    }
    public class TransportProposal {
        public int DeparturePortID { get; set; }
        public int DestinationPortID { get; set; }
        public decimal CostToDeparturePort { get; set; }
        public decimal OceanFreight { get; set; }
        public decimal CostToFinalDestination { get; set; }
    }
    public class PortMatrixInfo {
        public int DeparturePortID { get; set; }
        public int DestinationPortID { get; set; }
        public int UnitsPerContainer { get; set; }
        public decimal Cost { get; set; }
    }
    public class Shipping {
        public string ID { get; set; }
        public string Name { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutosVue.Models {

    public enum auctionStatus{
        NotSet = 0,
        Completed = 1,
        InProgress = 2,
        NotAssigned = 3
    }
    public class Auctions {
        public int AuctionID { get; set; }
        public int Code { get; set; }
        public int CompanyID { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZIP { get; set; }
        public string Street { get; set; }
        public string Phone { get; set; }
        public string ManagerName { get; set; }
        public string ManagerEmail { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string BranchImageUrl { get; set; }
        public string SuggesionName { get; set; }
        public bool OpenToPublic { get; set; }
    }

    public class AuctionSchedule {
        public int ID { get; set; }
        public int Code { get; set; }
        public DateTime StartTime { get; set; }
    }

    public class AuctionStatus {
        public int ID { get; set; }
        public string Description { get; set; }
    }
}
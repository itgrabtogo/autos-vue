﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutosVue.Models.IAAI {

    public class VehicleDetails {
        public string RestrictedTotoalCount { get; set; }
        public string ProductDetailsurl { get; set; }
        public VehicleDetailsViewModel VehicleDetailsViewModel { get; set; }
        public LightsAnnouncementsViewModel LightsAnnouncementsViewModel { get; set; }
        public bool OpenBlendedTermsPopup { get; set; }
        public string TermsOfUseText { get; set; }
        public bool EnableRecommendations { get; set; }
        public string CurrentTimeinCST { get; set; }
        public List<LstAnnouncementAlertInfo> lstAnnouncementAlertInfo { get; set; }
    }

    public class SimilarVehicle {
        public int ItemId { get; set; }
        public int BranchId { get; set; }
        public string BranchName { get; set; }
        public string StockNo { get; set; }
        public int Year { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public int SalvageID { get; set; }
        public string Series { get; set; }
    }

    public class VehicleDisplayInfo {
        public string Name { get; set; }
        public string DisplayText { get; set; }
        public int DisplayPattern { get; set; }
        public List<VehicleDisplayInfoValue> DisplayValues { get; set; }
    }
    
    public class VehicleDisplayInfoValue {
        public string Label { get; set; }
        public string Meta { get; set; }
        public string Text { get; set; }
        public bool Highlight { get; set; }
    }

    public class SaleInfo {
        public string StockNumber { get; set; }
        public string Lane { get; set; }
        public string Slot { get; set; }
        public string TitleDocument { get; set; }
        public string TitleState { get; set; }
        public string Brand { get; set; }
        public string Notes { get; set; }
        public string Seller { get; set; }
        public string ConditionReport { get; set; }
        public string CostOfRepairDoc { get; set; }
        public string ACV { get; set; }
        public string EstimatedRepairCost { get; set; }
        public List<string> WhoCanBuy { get; set; }
        public bool TexasForeignBuyer { get; set; }
        public bool OhioForeignBuyer { get; set; }
        public string ModifiedDate { get; set; }
        public int SalvageID { get; set; }
        public string AsapMake { get; set; }
        public string AsapModel { get; set; }
    }

    public class RemarketingRecommendationBadges {
        public bool Proximity_badge { get; set; }
        public bool Prior_similar_damage_purchase_badge { get; set; }
        public bool Prior_make_model_age_purchase_badge { get; set; }
        public bool Prior_make_model_age_bid_badge { get; set; }
        public bool Moves_fast_in_market_badge { get; set; }
        public bool Within_historical_acv_range_badge { get; set; }
        public bool No_Reserve_badge { get; set; }
        public bool Available_Now_badge { get; set; }
    }

    public class VehicleDetailsViewModel {
        public string AFCCountry { get; set; }
        public string AFCState { get; set; }
        public string BidcardMessageinSaleInfo { get; set; }
        public string FormattedMyMax { get; set; }
        public string IBFSoldTime { get; set; }
        public string IBFSoldMessage { get; set; }
        public bool IsPrebiddingDone { get; set; }
        public bool IsHighPrebidder { get; set; }
        public string AuctionStatus { get; set; }
        public string AuctionStatusDescription { get; set; }
        public bool DisplayMoreLinkForRemote { get; set; }
        public string RemoteSaleInfo { get; set; }
        public string RemoteSaleInfoTooltip { get; set; }
        public string PrebidPopupErrorMessage { get; set; }
        public string IBFPopupErrorMessage { get; set; }
        public string IBFAwardMesssage { get; set; }
        public string PrebidAwardMesssage { get; set; }
        public string IBFAwardedText { get; set; }
        public string OutBidAmountNeededText { get; set; }
        public string StartingBidAmountNeededText { get; set; }
        public string LocationTooltip { get; set; }
        public string BrokersTooltip { get; set; }
        public string FinanceTooltip { get; set; }
        public string ConditionCheckTooltip { get; set; }
        public string SaleInfoTooltip { get; set; }
        public bool IsSpeciality { get; set; }
        public bool DisplaySalesTaxWarning { get; set; }
        public string TransportationTooltip { get; set; }
        public string BiddingErrorMessage { get; set; }
        public string ErrorMessage { get; set; }
        public string AuctionHours { get; set; }
        public string AuctionMinutes { get; set; }
        public string Meridian { get; set; }
        public string TimedAuctionMeridian { get; set; }
        public string LiveDate { get; set; }
        public string LiveDateinUserTimeZone { get; set; }
        public string ETOBinUserTimeZone { get; set; }
        public string TimedAuctionDateinUserTimeZone { get; set; }
        public string CurrentDay { get; set; }
        public string CurrentMonth { get; set; }
        public string PrebidPickUpDate { get; set; }
        public string PrebidPayDate { get; set; }
        public string IBFPickUpDate { get; set; }
        public string IBFPayDate { get; set; }
        public string UserTimezoneAbb { get; set; }
        public bool UserLoginStatus { get; set; }
        public bool IsGuest { get; set; }
        public string IaaBranchCodeForSpeciality { get; set; }
        public string IaaBranchCodeForVirtualBranch { get; set; }
        public string IaaBranchLocationForSpeciality { get; set; }
        public string IaaBranchLocationForVirtualBranch { get; set; }
        public bool IsVehicleAtBranch { get; set; }
        public string IsVehicleAtIAABranchForSpeciality { get; set; }
        public string IsVehicleAtIAABranchForVirualBranch { get; set; }
        public string PrebidClosed { get; set; }
        public bool PrebidAllowed { get; set; }
        public bool IbuyFastAllowed { get; set; }
        public bool WatchingAllowed { get; set; }
        public bool IsWatching { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public int Year { get; set; }
        public int ItemID { get; set; }
        public int AuctionID { get; set; }
        public string DecimalHighBidAmount { get; set; }
        public decimal? DecimalOutBidAmount { get; set; }
        public string AuctionTypeID { get; set; }
        public string Code { get; set; }
        public string StringOutBidAmount { get; set; }
        public bool DisplayHighPrebid { get; set; }
        public string WatchTextToTrack { get; set; }
        public string LoginURL { get; set; }
        public string Metroid { get; set; }
        public string BidText { get; set; }
        public bool DisplayDeletedBidMsg { get; set; }
        public string Loss { get; set; }
        public string PrimaryDamage { get; set; }
        public string SecondaryDamage { get; set; }
        public string Odometer { get; set; }
        public string StartCode { get; set; }
        public string Keys { get; set; }
        public string FuelType { get; set; }
        public string Cylinders { get; set; }
        public string Engine { get; set; }
        public string Transmission { get; set; }
        public string DriveLineType { get; set; }
        public string Vehicle { get; set; }
        public string VehicleStatus { get; set; }
        public string VIN { get; set; }
        public string VINStatus { get; set; }
        public string BodyStyle { get; set; }
        public string VehicleClass { get; set; }
        public string Series { get; set; }
        public string ManufacturedIn { get; set; }
        public string ExteriorColor { get; set; }
        public string InteriorColor { get; set; }
        public string RestraintSystem { get; set; }
        public string AirBags { get; set; }
        public string Driver_Passenger { get; set; }
        public string Left_RightSide { get; set; }
        public string DynamicFeatures { get; set; }
        public string StockNo { get; set; }
        public string BranchName { get; set; }
        public string BranchAbb { get; set; }
        public string UserId { get; set; }
        public string ItemNo { get; set; }
        public string AuctionLane { get; set; }
        public string opendate { get; set; }
        public string SaleDoc { get; set; }
        public bool IsRunAndDrive { get; set; }
        public bool IsVehicleStarts { get; set; }
        public bool IsPublic { get; set; }
        public bool VisibleBuyerFeeLink { get; set; }
        public bool IsShrinkWrap { get; set; }
        public string StaticFeatures { get; set; }
        public string PreBidHistory { get; set; }
        public List<SimilarVehicle> SimilarVehicles { get; set; }
        public bool ShowCurrentBid { get; set; }
        public bool ShowOutbidMessage { get; set; }
        public bool ShowPrebidHistory { get; set; }
        public string HighBidAmount { get; set; }
        public string HighBidder { get; set; }
        public string LosingBidStatus { get; set; }
        public string MyStock { get; set; }
        public string MyCurrent { get; set; }
        public string MyMax { get; set; }
        public string OutBidAmount { get; set; }
        public string IncrementAmount { get; set; }
        public string BidCountText { get; set; }
        public string OriginalMaxAmount { get; set; }
        public string AdjustedCloseDate { get; set; }
        public int SalvageID { get; set; }
        public int BranchCode { get; set; }
        public int BranchId { get; set; }
        public string Slot { get; set; }
        public string AuctionDate { get; set; }
        public bool ShowPublicIcon { get; set; }
        public bool ShowIBidLiveIcon { get; set; }
        public bool BuyNowInd { get; set; }
        public double? BuyNowAmount { get; set; }
        public string BuyNowCloseDate { get; set; }
        public bool BuyNowExpired { get; set; }
        public bool BuyNowSold { get; set; }
        public bool ReserveMet { get; set; }
        public string BuyNowPrice { get; set; }
        public string Day { get; set; }
        public string Date { get; set; }
        public string Month { get; set; }
        public string Time { get; set; }
        public string TimedAuctionDay { get; set; }
        public string TimedAuctionDate { get; set; }
        public string TimedAuctionMonth { get; set; }
        public string TimedAuctionTime { get; set; }
        public string LiveDateDOWString { get; set; }
        public string LiveDateString { get; set; }
        public string TimedAuctionDateString { get; set; }
        public string WhoCanBid { get; set; }
        public string Brand { get; set; }
        public string ACV { get; set; }
        public string Notes { get; set; }
        public string ProviderName { get; set; }
        public string ECRDocLink { get; set; }
        public string VCRDocLink { get; set; }
        public bool IsECRDocLinkVisible { get; set; }
        public bool IsVCRDocLinkVisible { get; set; }
        public string BranchLink { get; set; }
        public string LocationName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Phone { get; set; }
        public string PickupStateZipPhone { get; set; }
        public string RemoteSaleAdditionInformation { get; set; }
        public int FreeStorageDays { get; set; }
        public bool IsPreBidVisible { get; set; }
        public bool? IsIBuyFastVisible { get; set; }
        public string ImageURL { get; set; }
        public string TImageURL1 { get; set; }
        public string TImageURL2 { get; set; }
        public string TImageURL3 { get; set; }
        public string TImageURL4 { get; set; }
        public string TImageURL5 { get; set; }
        public string TImageURL6 { get; set; }
        public string TImageURL7 { get; set; }
        public string TImageURL8 { get; set; }
        public string TImageURL9 { get; set; }
        public string TImageURL10 { get; set; }
        public List<VehicleDisplayInfo> ConditionInfo { get; set; }
        public List<VehicleDisplayInfo> VINInfo { get; set; }
        public List<string> OverviewInfo { get; set; }
        public List<VehicleDisplayInfo> ConditionOverviewInfo { get; set; }
        public List<VehicleDisplayInfo> ConditionOverViewAttributesInfo { get; set; }
        public List<VehicleDisplayInfo> VehicleInformation { get; set; }
        public List<VehicleDisplayInfo> VehicleDescription { get; set; }
        public SaleInfo SaleInfo { get; set; }
        public string DisplayCostOfRepairDoc { get; set; }
        public string DisplayConditionReport { get; set; }
        public bool DisplayPremiumReport { get; set; }
        public bool DisplayInstaVinReport { get; set; }
        public string TooltipMessage { get; set; }
        public bool IsImageRequestQualified { get; set; }
        public string ImageRequestDetails { get; set; }
        public bool IsHaulMatchVisible { get; set; }
        public string HaulmatchUrl { get; set; }
        public bool IsCarsArriveVisible { get; set; }
        public string CarsArriveUrl { get; set; }
        public bool IsAuctionlogoExport { get; set; }
        public bool IsAFCCaliforniaVisible { get; set; }
        public bool IsAFCUSVisible { get; set; }
        public string SalesTaxWarningMessage { get; set; }
        public bool DisplayTaxLink { get; set; }
        public bool DisplaySalesTaxWarningLinks { get; set; }
        public bool IsVirtualBranch { get; set; }
        public bool IsVehicleAtBranchVB { get; set; }
        public bool IsVenderSectionVisible { get; set; }
        public bool EvmMatchInd { get; set; }
        public string ASAPMake { get; set; }
        public string ASAPModel { get; set; }
        public string Seller { get; set; }
        public bool ConditionReportAllowed { get; set; }
        public bool CostOfRepairDocAllowed { get; set; }
        public bool PartsInfoExist { get; set; }
        public bool StockDisplayInd { get; set; }
        public string VTypeForInsta { get; set; }
        public string StorageLocationId { get; set; }
        public bool ChromeDataInd { get; set; }
        public int PromotionID { get; set; }
        public string PromotionTag { get; set; }
        public string PromotionUrl { get; set; }
        public bool AdditionalImages_Ind { get; set; }
        public bool IsBuyer { get; set; }
        public bool AdditionalImagesPurchasedInd { get; set; }
        public bool SellerPromotionInd { get; set; }
        public bool EnableAdditionalImagePurchase { get; set; }
        public bool TimedAuctionInd { get; set; }
        public string TimedAuctionCloseTimeCST { get; set; }
        public bool IsCatVehicle { get; set; }
        public double StartBid { get; set; }
        public string StorageLocationLatitude { get; set; }
        public string StorageLocationLongitude { get; set; }
        public string CATVehicleAdditionalDetailsURL { get; set; }
        public bool ShowCATVehicleAdditionalDetails { get; set; }
        public bool ShowDrivingDirections { get; set; }
        public int TimedAuctionBuyNowOfferstatus { get; set; }
        public double MinimumBidAmount { get; set; }
        public int BuyNowOfferAmount { get; set; }
        public string TimedAuctionClosingStatus { get; set; }
        public RemarketingRecommendationBadges RemarketingRecommendationBadges { get; set; }
        public string VehicleType { get; set; }
        public string TimedAuctionSoldTime { get; set; }
        public string DisturbingImageId { get; set; }
        public bool VehicleStickerIndicator { get; set; }
    }

    public class Lights {
        public int SalvageLightId { get; set; }
        public int SalvageId { get; set; }
        public bool GreenInd { get; set; }
        public bool RedInd { get; set; }
        public bool YellowInd { get; set; }
        public bool BlueInd { get; set; }
        public string CustomAnnouncement { get; set; }
    }

    public class LightsAnnouncementsViewModel {
        public Lights lights { get; set; }
        public List<string> Announcements { get; set; }
    }

    public class LstAnnouncementAlertInfo {
        public int Auction_Announcement_Info_ID { get; set; }
        public string Message_Type { get; set; }
        public string Message_Title { get; set; }
        public string Message { get; set; }
        public DateTime Effective_Date { get; set; }
        public DateTime Expiration_Date { get; set; }
        public int Branchid { get; set; }
        public DateTime Auctiondate { get; set; }
        public string Branchname { get; set; }
    }

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutosVue.API.Models {

	public class SelectedProposal {
		public int ID { get; set; }
		public int DeparturePortID { get; set; }
		public int DestinationPortID { get; set; }
	}

	public class BooleanFieldUpdate {
		public int ID { get; set; }
		public bool Value { get; set; }
	}

	public class DecimalFieldUpdate {
		public int ID { get; set; }
		public decimal Value { get; set; }
	}

}
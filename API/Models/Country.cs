﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutosVue.Models {
	public class Country {
		public int ID { get; set; }
		public string Name { get; set; }
		public string Code { get; set; }
		public string CurrencyCode { get; set; }
		public string CurrencyName { get; set; }
		public string Capital { get; set; }
		public string ContinentCode { get; set; }
		public string Continent { get; set; }
		public string PhoneCode { get; set; }
		public string Domain { get; set; }
	}
}
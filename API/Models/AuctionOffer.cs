﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dapper.Contrib.Extensions;

namespace AutosVue.Models {

	public static class DutyFees {
		public const decimal DutyFeeMultiplier = 0.32m;
		public const decimal PriceAdjusmentMultipler = 0.75m;
	}

	[Table( "AuctionOffers" )]
	public class AuctionOffer {
		[Key]
		public long ID { get; set; }
		public int CompanyID { get; set; }
		public int LocationID { get; set; }
		public int AuctionID { get; set; }
		public int ItemID { get; set; }
		public string Make { get; set; }
		public string ItemLink { get; set; }
		public string Model { get; set; }
		public int Year { get; set; }
		public string CountryOfOrigin { get; set; }
		public string Transmission { get; set; }
		public string DriveLineType { get; set; } //All Wheel Drive
		public string Fuel { get; set; }
		public string Engine { get; set; }
		public string Odometer { get; set; }
		public string Title { get; set; }
		public string StartCode { get; set; }
		public string Loss { get; set; }
		public string PrimaryDamage { get; set; }
		public string SecondaryDamage { get; set; }
		public bool IsAirbagDeployed { get; set; }
		public bool IsKeyPresent { get; set; }
		public string Seller { get; set; }
		public string SellingBranch { get; set; }
		public string VIN { get; set; }
		public int VehicleTypeID { get; set; }
		public DateTime? AuctionLiveDate { get; set; }
		public decimal CurrentBidAmount { get; set; }
		public decimal MinimumBidAmount { get; set; }
		public decimal BuyItAmount { get; set; }
		public DateTime? DateAcquired { get; set; }
		public decimal AmountPaid { get; set; }
		public decimal AuctionFeeAmount { get; set; }
		public decimal USTransportCost { get; set; }
		public decimal OceanFreightCost { get; set; }
		public decimal EUTransportCost { get; set; }
		public decimal DutyVAT { get; set; }
		public decimal EstimatedRepairCost { get; set; }
		public decimal EstimatedPaperworkCost { get; set; }
		public decimal EstimatedSaleAmount { get; set; }


		[Write( false )]
		public decimal EstimatedProfit => EstimatedSaleAmount - EstimatedGrandTotal; // - shipping

		[Write( false )]
		public decimal EstimatedGrandTotal => BidAmount + EstimatedRepairCost + EstimatedPaperworkCost + DutyVAT + USTransportCost + EUTransportCost + OceanFreightCost;

		public int AuctionStatusID { get; set; }
		//public string AuctionCity { get; set; }
		//public string AuctionState { get; set; }
		//public string AuctionZip { get; set; }

		public int DeparturePortID { get; set; }
		public int DestinationPortID { get; set; }

		public bool IsSelected { get; set; }

		[Write( false )]
		public List<TransportProposal> TransportProposals { get; set; }

		[Write( false )]
		public List<PortMatrixInfo> PortMatrix { get; set; }

		[Write( false )]
		public string AuctionStatus { get; set; }

		[Write( false )]
		public int proposalDeparturePortID { get; set; }
		[Write( false )]
		public int proposalDestinationPortID { get; set; }
		[Write( false )]
		public decimal proposalCostToDeparturePort { get; set; }
		[Write( false )]
		public decimal proposalOceanFreight { get; set; }
		[Write( false )]
		public decimal proposalCostToFinalDestination { get; set; }

		[Write( false )]
		public int transportMatrixUnitsPerContainer { get; set; }
		[Write( false )]
		public decimal transportMatrixCost { get; set; }

		public decimal BidAmount { get; set; }
	}

	public class VehicleType {
		public int ID { get; set; }
		public string Type { get; set; }
	}

}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutosVue.Models {
	public class TimeZoneInfo {
		public string WindowsTimeZone { get; set; }
		public string Daylight { get; set; }
		public string Standard { get; set; }
		public string IANATimeZone { get; set; }
	}
}
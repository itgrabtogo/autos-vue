﻿using System;
using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace AutosVue.API {

    public static class WebApiConfig {
        public static void Register( HttpConfiguration config ) {
            // Web API routes
            config.MapHttpAttributeRoutes();

            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "api/{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //);


            // set default formatter to json
            var jsonFormatter = config.Formatters.JsonFormatter;
            var contractResolver = (DefaultContractResolver) jsonFormatter.SerializerSettings.ContractResolver;
            contractResolver.IgnoreSerializableAttribute = true;
            jsonFormatter.SerializerSettings = new JsonSerializerSettings {
                Formatting = Formatting.None,
                TypeNameHandling = TypeNameHandling.None,
                ContractResolver = new DefaultContractResolver(),
				ObjectCreationHandling = ObjectCreationHandling.Replace,
            };

            jsonFormatter.MediaTypeMappings
                .Add( new System.Net.Http.Formatting.RequestHeaderMapping( "Accept",
                    "text/html",
                    StringComparison.InvariantCultureIgnoreCase,
                    true,
                    "application/json" ) );
        }
    }
}
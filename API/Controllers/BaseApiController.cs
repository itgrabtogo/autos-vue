﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.WebPages;
using Amazon.Runtime.Internal.Util;
using AutosVue.API.Code;

namespace AutosVue.API.Controllers {

	public class BaseApiController : ApiController {
		
		private const int MAX_API_RETRIES = 5;

		IDbConnection m_db;
		public IDbConnection db {
			get {
				if ( m_db == null ) {
					m_db = DB.CreateMSSQL();
					m_db.Open();
				}
				return m_db;
			}
			set {
				m_db = value;
			}
		}

		private void DisposeDB() {
			if ( m_db != null ) {
				m_db.Dispose();
				m_db = null;
			}
		}

		public override Task<HttpResponseMessage> ExecuteAsync( HttpControllerContext controllerContext, CancellationToken cancellationToken ) {
			if ( controllerContext.Request.Method == HttpMethod.Options ) {
				var response = new HttpResponseMessage( HttpStatusCode.OK ) { Content = new StringContent( String.Empty ) };
				response.Headers.Add( "Access-Control-Allow-Methods", "GET, POST, PUT, DELETE" );
				response.Headers.Add( "Access-Control-Allow-Headers", "Content-Type, Accept, Authorization" );
				response.Headers.Add( "Access-Control-Allow-Credentials", "true" );
				response.Headers.Add( "Access-Control-Max-Age", "1728000" );
				return Task.FromResult( response );
			} else {
				return base.ExecuteAsync( controllerContext, cancellationToken ).ContinueWith( x => {
					DisposeDB();
					return x.Result;
				}, cancellationToken );
			}
		}

		protected override void Dispose( bool disposing ) {
			DisposeDB();
			base.Dispose( disposing );
		}

		public HttpResponseMessage UnauthorizedRespose( string msg = "Invalid credentials" ) {
			return Request.CreateErrorResponse( HttpStatusCode.Unauthorized, msg );
		}

		public HttpResponseMessage InternalServerError( Exception err, string msg = "Internal Server Error!" ) {
#if DEBUG
			return Request.CreateResponse( HttpStatusCode.InternalServerError, new { Message = msg, Exception = err } );
#else
			return Request.CreateErrorResponse( HttpStatusCode.InternalServerError, msg );
#endif
		}

		public new HttpResponseMessage Ok<T>( T responseData ) {
			return Request.CreateResponse( HttpStatusCode.OK, responseData );
		}


		protected string SendRequest( string url, NameValueCollection parameters ) {
			for ( var attempt = 0; attempt < MAX_API_RETRIES; attempt++ ) {
				try {
					var uri = new UriBuilder( url );

					uri.Query = string.Join( "&",
						parameters.AllKeys.Select( key => string.Format( "{0}={1}", HttpUtility.UrlEncode( key ), HttpUtility.UrlEncode( parameters[ key ] ) ) ) );

					var request = (HttpWebRequest) WebRequest.Create( uri.ToString() );
					request.AutomaticDecompression = DecompressionMethods.GZip;
					request.Timeout = 5 * 1000;
					var resp = request.GetResponse().GetResponseStream();
					if ( resp != null ) {
						return new StreamReader( resp ).ReadToEnd();
					} else {
						return null;
					}
				} catch ( Exception err ) {
					if ( err is System.Net.WebException && attempt < MAX_API_RETRIES ) {
						var ex = (System.Net.WebException) err;
						if ( err.Message.IndexOf( "timed out" ) > -1 ) continue;
					}
					throw;
				}
			}

			return null;
		}

	}
}
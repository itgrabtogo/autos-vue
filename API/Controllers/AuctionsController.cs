﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Collections.Generic;
using System.Data;
using Dapper;
using System.Linq;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Web;
using System.Threading.Tasks;
using AutosVue.Models;
using AutosVue.API.Models;

namespace AutosVue.API.Controllers {

    [RoutePrefix( "api/auctions" )]
    public class AuctionsController : BaseApiController {


	    private Dictionary<int, string> m_allPorts = null;

	    public Dictionary<int, string> allPorts {
		    get {
			    if ( m_allPorts == null ) {
				    m_allPorts = db.Query<Port>( "select * from Ports" ).ToDictionary( x => x.ID, x => x.Name );
			    }

			    return m_allPorts;
		    }
	    }

		[HttpGet]
		[Route( "list" )]
        public HttpResponseMessage List() {
			try {
				var allVehicleTypes = db.Query<VehicleType>( "select * from VehicleTypes" ).ToList();
				var activeOffers = db.Query<AuctionOffer>( @"
						SELECT AuctionOffers.*,
								ISNULL(AuctionStatus.Description,'N/A') AS AuctionStatus,
								Auctions.City AS AuctionCity,
								Auctions.State AS AuctionState,
								Auctions.ZIP AS AuctionZip,
								TransportProposals.DeparturePortID as proposalDeparturePortID,
								TransportProposals.DestinationPortID as proposalDestinationPortID,
								TransportProposals.CostToDeparturePort as proposalCostToDeparturePort,
								TransportProposals.OceanFreight as proposalOceanFreight,
								TransportProposals.CostToFinalDestination as proposalCostToFinalDestination,
								PortMatrix.UnitsPerContainer as transportMatrixUnitsPerContainer,
								PortMatrix.Cost as transportMatrixCost
						FROM AuctionOffers
							INNER JOIN Auctions
								ON Auctions.LocationID = AuctionOffers.LocationID
							LEFT JOIN AuctionStatus
								ON AuctionOffers.AuctionStatusID = AuctionStatus.ID
							LEFT JOIN TransportProposals
								ON AuctionOffers.ID=TransportProposals.OfferID
							LEFT JOIN PortMatrix
								ON PortMatrix.DeparturePortID = TransportProposals.DeparturePortID 
								AND PortMatrix.DestinationPortID = TransportProposals.DestinationPortID
								AND PortMatrix.IsAllowed=1
						WHERE
							AuctionOffers.Active=1 AND 
							AuctionOffers.CompanyID=1
					" )
							.ToList()
							.GroupBy( x => x.ID, x => x ).Select( x => {
								var rec = x.First();
								rec.TransportProposals = x.GroupBy( t => new { t.proposalDeparturePortID, t.proposalDestinationPortID }, t => t ).Select( t => new TransportProposal() {
									DeparturePortID = t.First().proposalDeparturePortID,
									DestinationPortID = t.First().proposalDestinationPortID,
									CostToDeparturePort = t.First().proposalCostToDeparturePort,
									OceanFreight = t.First().proposalOceanFreight,
									CostToFinalDestination = t.First().proposalCostToFinalDestination
								} ).ToList();

								rec.PortMatrix = x.GroupBy( t => new { t.proposalDeparturePortID, t.proposalDestinationPortID, t.transportMatrixUnitsPerContainer }, t => t ).Select( t => new PortMatrixInfo() {
									DeparturePortID = t.Key.proposalDeparturePortID,
									DestinationPortID = t.Key.proposalDestinationPortID,
									UnitsPerContainer = t.Key.transportMatrixUnitsPerContainer,
									Cost = t.First().transportMatrixCost
								} ).ToList();
								return rec;
							} ).ToList();
				
				var containerInfo = RecalcContainerCost( activeOffers.Where( x => x.IsSelected ).ToList() );
				
				return Ok( new { activeOffers, containerInfo } );
			} catch ( Exception err ) {
				return InternalServerError( err );
			}
		}

		#region Container Cost Calculation
		public class ContainerInfoViewModel {
			public int DeparturePortID { get; set; }
			public int DestinationPortID { get; set; }
			public string ContainerTitle { get; set; }
			public int MaxContainerUnits { get; set; }
			public int ActualContainerUnits { get { return Cars.Count(); } }
			public List<AuctionOffer> Cars { get; set; }

			public ContainerInfoViewModel( int departurePortId, int destinationPortId, string title, int units ) {
				DeparturePortID = departurePortId;
				DestinationPortID = destinationPortId;
				ContainerTitle = title;
				MaxContainerUnits = units;
				Cars = new List<AuctionOffer>();
			}
		}

		private List<ContainerInfoViewModel> RecalcContainerCost( List<AuctionOffer> lst ) {
			var ContainerList = new List<ContainerInfoViewModel>();

			var data = lst.Where( x => x.IsSelected ).GroupBy( x => new { x.DeparturePortID, x.DestinationPortID }, x => x ); // group selected autos by departure & destination port
			foreach ( var container in data ) {
				// num cars in this container
				var numCars = container.Count();

				// get container information from portmatrix
				var containerInfo = container.First().PortMatrix
					.Where( x => x.DeparturePortID == container.Key.DeparturePortID && x.DestinationPortID == container.Key.DestinationPortID && x.UnitsPerContainer <= numCars )
					.OrderByDescending( x => x.UnitsPerContainer ).First();

				// if departure and destination ports are not selected yet - ignore
				if ( containerInfo.UnitsPerContainer == 0 || container.Key.DeparturePortID == 0 ) continue;

				// create container info to display
				var thisContainer = new ContainerInfoViewModel(
					container.Key.DeparturePortID,
					container.Key.DestinationPortID,
					allPorts[ container.Key.DeparturePortID ] + " - " + allPorts[ container.Key.DestinationPortID ],
					containerInfo.UnitsPerContainer
				);

				// calc num containers
				var numContainers = numCars / containerInfo.UnitsPerContainer;
				var lastContainerCars = numCars % containerInfo.UnitsPerContainer;
				if ( lastContainerCars == 2 && numContainers > 1 ) { // transfer one car into last container to make them 3
					numContainers -= 1;
					lastContainerCars += 1;
				}
				foreach ( var car in container ) {
					// calculate container cost depending if it's the last container or not
					var numCarsInThisContainer = numContainers > 0 ? containerInfo.UnitsPerContainer : lastContainerCars;
					if ( numCarsInThisContainer != containerInfo.UnitsPerContainer ) { // find correct container info for last container
						containerInfo = container.First().PortMatrix.Where( x => x.UnitsPerContainer <= numCarsInThisContainer ).OrderByDescending( x => x.UnitsPerContainer ).First();
					}
					car.OceanFreightCost = containerInfo.Cost / numCarsInThisContainer;

					// also update proposal info
					var selectedProposal = car.TransportProposals.FirstOrDefault( x => x.DeparturePortID == car.DeparturePortID && x.DestinationPortID == car.DestinationPortID );
					if ( selectedProposal != null ) {
						selectedProposal.OceanFreight = car.OceanFreightCost;
					}

					// add car to this container
					thisContainer.Cars.Add( car );

					// if container is full - create next one
					if ( thisContainer.Cars.Count == thisContainer.MaxContainerUnits ) {
						ContainerList.Add( thisContainer );
						thisContainer = new ContainerInfoViewModel(
							container.Key.DeparturePortID,
							container.Key.DestinationPortID,
							allPorts[ container.Key.DeparturePortID ] + " - " + allPorts[ container.Key.DestinationPortID ],
							containerInfo.UnitsPerContainer
						);
						numContainers--;
					}
				}

				// add container if not empty
				if ( thisContainer.Cars.Any() ) {
					ContainerList.Add( thisContainer );
				}
			}


			// recalc EU transport cost
			var EUTransportByPort = ContainerList.GroupBy( x => new { x.DestinationPortID }, x => x ).ToList();
			foreach ( var EUPort in EUTransportByPort ) {
				// get possible EU transport
				var numCars = EUPort.Sum( x => x.Cars.Count() );
				var matrix = db.Query<TransportMatrix>( "SELECT * FROM TransportMatrix WHERE LocationID=-999 and PortID=@DestinationPortID and UnitsCount <= @NumCars", new {
					EUPort.Key.DestinationPortID,
					numCars
				} ).ToList();

				// group cars on trucks
				var addedCars = 0;
				var remainingCars = numCars;
				var selectedEUtransport = matrix.OrderByDescending( x => x.UnitsCount ).First( x => x.UnitsCount <= remainingCars );
				foreach ( var car in EUPort.SelectMany( x => x.Cars ) ) {
					car.EUTransportCost = selectedEUtransport.TransportCost / selectedEUtransport.UnitsCount;

					addedCars++;
					remainingCars--;

					if ( addedCars > selectedEUtransport.UnitsCount ) { // new truck
						addedCars = 0;
						selectedEUtransport = matrix.OrderByDescending( x => x.UnitsCount ).First( x => x.UnitsCount <= remainingCars );
					}
				}
			}

			return ContainerList;
		}
		#endregion


		#region Lookups
		[HttpGet]
		[Route( "lookups" )]
		public HttpResponseMessage Lookups() {
			try {
				return Ok( new {
					ports = db.Query<Port>( "select * from Ports" ),
					vehicleTypes = db.Query<VehicleType>( "select * from VehicleTypes" )
				} );
			} catch ( Exception err ) {
				return InternalServerError( err );
			}
		}
		#endregion

		#region Event Handling
		
		[HttpGet, HttpPost]
		[Route( "updateTransport" )]
		public HttpResponseMessage UpdateTransport( [FromBody] SelectedProposal rec ) {
			if ( rec.DeparturePortID > 0 ) {
				db.Execute( @"
						update AuctionOffers 
						set
							DeparturePortID=@DeparturePortID, 
							DestinationPortID=@DestinationPortID, 
							OceanFreightCost=tp.OceanFreight, 
							USTransportCost=tp.CostToDeparturePort,
							EUTransportCost=tp.CostToFinalDestination
						from 
							AuctionOffers o join TransportProposals tp on (o.ID = tp.OfferID)
						where o.ID=@ID and tp.DeparturePortID=@DeparturePortID and tp.DestinationPortID=@DestinationPortID;
					", new {
						rec.ID,
						rec.DeparturePortID,
						rec.DestinationPortID
					} );
			} else { 
				db.Execute( @"
						update AuctionOffers 
						set
							DeparturePortID=NULL, 
							DestinationPortID=NULL, 
							OceanFreightCost=NULL, 
							USTransportCost=NULL,
							EUTransportCost=NULL
						where ID=@ID 
					", new { rec.ID } );
			}

			return List();
		}

		[HttpGet, HttpPost]
		[Route( "UpdateSelected" )]
		public HttpResponseMessage UpdateSelected( [FromBody] BooleanFieldUpdate rec ) {
			db.Execute( "update AuctionOffers set IsSelected=@Value where ID=@ID", new { rec.ID, rec.Value } );
			return List();
		}

		[HttpGet, HttpPost]
		[Route( "UpdateBidAmount" )]
		public HttpResponseMessage UpdateBidAmount( [FromBody] DecimalFieldUpdate rec ) {
			var transport = db.ExecuteScalar<Decimal>( "select USTransportCost + OceanFreightCost + EUTransportCost from AuctionOffers where ID=@ID", new { rec.ID } );
			var DutyVAT = ( rec.Value + transport ) * DutyFees.DutyFeeMultiplier * DutyFees.PriceAdjusmentMultipler;
			db.Execute( "update AuctionOffers set BidAmount=@Value, DutyVAT=@DutyVAT where ID=@ID", new { rec.ID, DutyVAT, rec.Value } );
			return List();
		}

		[HttpGet, HttpPost]
		[Route( "UpdateAmountPaid" )]
		public HttpResponseMessage UpdateAmountPaid( [FromBody] DecimalFieldUpdate rec ) {
			db.Execute( "update AuctionOffers set AmountPaid = @Value where ID = @ID", new { rec.ID, rec.Value } );
			return List();
		}

		[HttpGet, HttpPost]
		[Route( "UpdateEstimatedRepairCost" )]
		public HttpResponseMessage UpdateEstimatedRepairCost( [FromBody] DecimalFieldUpdate rec ) {
			db.Execute( "update AuctionOffers set EstimatedRepairCost = @Value where ID = @ID", new { rec.ID, rec.Value } );
			return List();
		}
		
		[HttpGet, HttpPost]
		[Route( "UpdateEstimatedSaleAmount" )]
		public HttpResponseMessage UpdateEstimatedSaleAmount( [FromBody] DecimalFieldUpdate rec ) {
			db.Execute( "update AuctionOffers set EstimatedSaleAmount = @Value where ID = @ID", new { rec.ID, rec.Value } );
			return List();
		}


		#endregion

	}
}
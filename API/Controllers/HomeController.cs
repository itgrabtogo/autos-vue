﻿using System.Web.Mvc;

namespace AutosVue.API.Controllers {
	public class HomeController : Controller {

		public ActionResult Index() {
			return Redirect( "index.html" );
		}
	}
}
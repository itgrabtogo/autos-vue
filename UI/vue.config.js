module.exports = {
  assetsDir: 'assets',
  runtimeCompiler: true,
  productionSourceMap: false,
  parallel: undefined,
  css: undefined,

  configureWebpack: config => {
    if (process.env.NODE_ENV === 'production') {
      // mutate config for production...
    } else {
      // mutate for development...
    }
  }
}

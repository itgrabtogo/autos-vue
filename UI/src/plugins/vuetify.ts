import Vue from 'vue';
import Vuetify from 'vuetify/lib';

import colors from 'vuetify/lib/util/colors'

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: colors.blue.lighten1,
        secondary: colors.blue.lighten3,
        accent: colors.orange.darken1,
        error: colors.red.lighten1,
        warning: colors.orange.base,
        info: colors.blue.lighten1,
        success: colors.green.base
      },
      dark: {
        primary: colors.blue.lighten3,
      },
    },
  },
  icons: {
    iconfont: 'fa',
  }
});

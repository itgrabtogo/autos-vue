import {AxiosResponse} from 'axios'
import {API} from '@/code/api'

export interface Lookup {
    ID: number,
    Name: string,
    IsActive?: boolean
  }

// *****************************************************************************************************
// API
// *****************************************************************************************************
export class LookupAPI extends API {
  // async loadAllEmployees() {
  //   const response = await this.get(`Lookups/Employees`) as AxiosResponse<Lookup[]>
  //   return response.data
  // }
}

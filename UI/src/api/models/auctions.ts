import {AxiosResponse} from 'axios'
import {API} from '../../code/api'

export interface AuctionOffer {
  ID: number;
  CompanyID: number;
  LocationID: number;
  AuctionID: number;
  ItemID: number;
  Make: string;
  ItemLink: string;
  Model: string;
  Year: number;
  CountryOfOrigin: string;
  Transmission: string;
  DriveLineType: string;
  Fuel: string;
  Engine: string;
  Odometer: string;
  Title: string;
  StartCode: string;
  Loss: string;
  PrimaryDamage: string;
  SecondaryDamage: string;
  IsAirbagDeployed: boolean;
  IsKeyPresent: boolean;
  Seller: string;
  SellingBranch: string;
  VIN: string;
  VehicleTypeID: number;
  AuctionLiveDate: Date | string | null;
  CurrentBidAmount: number;
  MinimumBidAmount: number;
  BuyItAmount: number;
  DateAcquired: Date | string | null;
  AmountPaid: number;
  AuctionFeeAmount: number;
  USTransportCost: number;
  OceanFreightCost: number;
  EUTransportCost: number;
  DutyVAT: number;
  EstimatedRepairCost: number;
  EstimatedPaperworkCost: number;
  EstimatedSaleAmount: number;

  EstimatedProfit: number; // - shipping

  EstimatedGrandTotal: number;

  AuctionStatusID: number;
  // AuctionCity: string;
  // AuctionState: string;
  // AuctionZip: string;

  DeparturePortID: number;
  DestinationPortID: number;

  SelectedTransport: string;

  IsSelected: boolean;

  TransportProposals: TransportProposal[];

  PortMatrix: PortMatrixInfo[];

  AuctionStatus: string;

  proposalDeparturePortID: number;
  proposalDestinationPortID: number;
  proposalCostToDeparturePort: number;
  proposalOceanFreight: number;
  proposalCostToFinalDestination: number;

  transportMatrixUnitsPerContainer: number;
  transportMatrixCost: number;

  BidAmount: number;
}

export interface VehicleType {
  ID: number;
  Type: string;
}

export interface ContainerShipping {
  DeparturePortID: number;
  DestinationPortID: number;
  UnitsPerContainer: number;
  Cost: number;
}
export interface Port {
  ID: number;
  Name: string;
  Zip: string;
  Country: string;
}
export interface TransportMatrix {
  AuctionID: number;
  PortID: number;
  TransportCost: number;
  UnitsCount: number;
}
export interface TransportProposal {
  DeparturePortID: number;
  DestinationPortID: number;
  CostToDeparturePort: number;
  OceanFreight: number;
  CostToFinalDestination: number;
}
export interface PortMatrixInfo {
  DeparturePortID: number;
  DestinationPortID: number;
  UnitsPerContainer: number;
  Cost: number;
}
export interface Shipping {
  ID: string;
  Name: string;
}

export interface ContainerInfoViewModel {
  DeparturePortID: number;
  DestinationPortID: number;
  ContainerTitle: string;
  MaxContainerUnits: number;
  ActualContainerUnits: number;
  Cars: AuctionOffer[];

  DeparturePortName: string;
  DestinationPortName: string;
  USTransportCost: number;
  EUTransportCost: number;
  OceanFreightCost: number;
}

export interface AuctionData {
  activeOffers: AuctionOffer[]
  containerInfo: ContainerInfoViewModel[]
}

export interface Port {
  ID: number
  Name: string
  Zip: string
  Country: string
}

export interface VehicleType {
  ID: number
  Type: string
}

export interface LookupData {
  ports: Port[]
  vehicleTypes: VehicleType[]
}

// *****************************************************************************************************
// API
// *****************************************************************************************************
export class AuctionsAPI extends API {

  async loadAuctions() {
    const response = await this.get(`auctions/list`) as AxiosResponse<AuctionData>
    return response.data
  }

  async loadLookups() {
    const response = await this.get(`auctions/lookups`) as AxiosResponse<LookupData>
    return response.data
  }

  async updateTransport(itm: AuctionOffer) {
    const response = await this.post(`auctions/updateTransport`, {
                                      ID: itm.ID,
                                      DeparturePortID: itm.DeparturePortID,
                                      DestinationPortID: itm.DestinationPortID
                                    }) as AxiosResponse<AuctionData>

    return response.data
  }

  async updateEstimatedSaleAmount(itm: AuctionOffer) {
    const response = await this.post(`auctions/updateEstimatedSaleAmount`, {
                                      ID: itm.ID,
                                      Value: itm.EstimatedSaleAmount
                                    }) as AxiosResponse<AuctionData>

    return response.data
  }

  async updateBidAmount(itm: AuctionOffer) {
    const response = await this.post(`auctions/updateBidAmount`, {
                                      ID: itm.ID,
                                      Value: itm.BidAmount
                                    }) as AxiosResponse<AuctionData>

    return response.data
  }

  async updateAmountPaid(itm: AuctionOffer) {
    const response = await this.post(`auctions/UpdateAmountPaid`, {
                                      ID: itm.ID,
                                      Value: itm.AmountPaid
                                    }) as AxiosResponse<AuctionData>

    return response.data
  }

  async updateEstimatedRepairCost(itm: AuctionOffer) {
    const response = await this.post(`auctions/UpdateEstimatedRepairCost`, {
                                      ID: itm.ID,
                                      Value: itm.EstimatedRepairCost
                                    }) as AxiosResponse<AuctionData>

    return response.data
  }

  async updateSelected(ID: number, selected: boolean) {
    const response = await this.post(`auctions/updateSelected`, {
      ID,
      Value: selected
    }) as AxiosResponse<AuctionData>

    return response.data
  }

}

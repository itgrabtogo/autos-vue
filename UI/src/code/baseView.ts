import { Component, Vue, Prop } from 'vue-property-decorator'
import { State, Getter, Mutation, Action } from 'vuex-class'
import formatDate from 'date-fns/format'
import parseISO from 'date-fns/parseISO'
import subMonths from 'date-fns/subMonths'
import addDays from 'date-fns/addDays'
import startOfMonth from 'date-fns/startOfMonth'
import endOfMonth from 'date-fns/endOfMonth'

export const longPressDelay: number = 1000

@Component({
})
export class BaseView extends Vue {
  @Mutation showSnackbar: any
  @Mutation logout: any

  longPressedButtonClickedAt: Date = new Date()
  longPressButtonIcon: string = ''
  longPressButtonOldIcon: string = ''
  longPressButtonProgressIndicator?: string = ''

  isEmptyTime( date: number | Date | string | undefined | null ) {
    return (date === undefined || date === null || date === '1900-01-01T00:00:00' || date === '0001-01-01T00:00:00');
  }

  formatDate(date: number | Date | string, fmt: string = 'MM/dd/yyyy') {
    if (this.isEmptyTime(date)) {
      return '';
    }
    if (typeof date === 'string') {
      date = parseISO(date)
    }
    return formatDate(date, fmt)
  }

  formatDatePicker(date: Date | string | undefined | null, fmt: string = 'yyyy-MM-dd') {
    if (this.isEmptyTime(date)) {
      return '';
    }
    if (typeof date === 'string') {
      date = parseISO(date)
    }
    return formatDate(date!, fmt)
  }

  formatTimePicker(date: Date | string | undefined | null, fmt: string = 'hh:mm') {
    if (this.isEmptyTime(date)) {
      return '';
    }
    if (typeof date === 'string') {
      date = parseISO(date)
    }
    return formatDate(date!, fmt)
  }

  formatTime(date: Date | string | null | undefined, fmt: string = 'hh:mm a') {
    if (this.isEmptyTime(date)) {
      return '';
    }
    if (typeof date === 'string') {
      date = parseISO(date)
    }
    return formatDate(date!, fmt)
  }

  formatTimeStr(time: string) {
    if (!time || time.length < 5) {
      return '00:00'
    }
    return time.substring(0, 5)
 }

  formatMoney(n: number, currencySign: string = '') {
    if (n === 0) {
      return '-';
    }
    return n.toFixed(2);
  }

  formatNumber(n: number, len: number) {
    if (n === 0 || isNaN(n)) {
      return '-';
    }
    let s: string = n.toString();
    while (s.length < len) {
      s = '0' + s;
    }
    return s;
  }

  formatNumericString(n: string, len: number) {
    if ( n === undefined || n === '' || n === null || !Number(n) ) {
        return '-';
    }
    let s: string = n;
    while (s.length < len) {
        s = '0' + s;
    }
    return s;
  }

  longPressStart(buttonIcon: string, progressIndicator: string) {
    this.longPressedButtonClickedAt = new Date()
    if (!buttonIcon) {
      return
    }

    this.longPressButtonIcon = buttonIcon
    this.longPressButtonProgressIndicator = progressIndicator

    // @ts-ignore
    this.longPressButtonOldIcon = this[ this.longPressButtonIcon ]
    // @ts-ignore
    this[ this.longPressButtonIcon ] = 'fas fa-spinner fa-pulse'
    setTimeout( () => {
      if (this.longPressButtonIcon) {
        // @ts-ignore
        this[ this.longPressButtonIcon ] = 'fas fa-check-circle fa-spin'
        // @ts-ignore
        this[ this.longPressButtonProgressIndicator ] = 100
      }
    }, longPressDelay)

    if (progressIndicator) {
      let progressPct = 0
      const self = this
      const numSteps = 20
      const progressUpdate = () => {
        if (self.longPressButtonProgressIndicator) {
          progressPct += 100 / numSteps
          // @ts-ignore
          self[ self.longPressButtonProgressIndicator ] = progressPct
          setTimeout( progressUpdate, longPressDelay / numSteps)
        }
      }
      setTimeout( progressUpdate, longPressDelay / numSteps)
    }
  }

  longPressEnd(callback: any, shortPressCallback: any) {
    if (this.longPressButtonIcon) {
      // @ts-ignore
      this[ this.longPressButtonIcon ] = this.longPressButtonOldIcon
      this.longPressButtonIcon = ''
    }

    if (this.longPressButtonProgressIndicator) {
      // @ts-ignore
      this[ this.longPressButtonProgressIndicator ] = 0
      this.longPressButtonProgressIndicator = ''
    }

    if (callback == null) {
      return
    }

    const ms = new Date().getTime() - this.longPressedButtonClickedAt.getTime()
    if (ms < longPressDelay) {
      if (shortPressCallback != null) {
        shortPressCallback()
      }
      return
    }

    callback()
  }

  onLogout() {
    this.logout()
    this.$router.push({ name: 'login' })
  }

}

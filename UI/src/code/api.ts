import { Commit } from 'vuex/types'
import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios'
import { RootState } from './rootState';
import { ValidationError, ServerError, NetworkError, UnauthorizedRequestError, ServerException } from './errors'

// tslint:disable:max-classes-per-file

export interface VuexInterface {
    state: RootState
    commit: Commit
}

export class API {
    client: AxiosInstance
    store: VuexInterface

    constructor(store: VuexInterface,
                baseURL: string = process.env.VUE_APP_API_ROOT || '',
                timeout: number = 12000) {
        this.client = axios.create({ baseURL, timeout })
        this.store = store
    }

    protected get<T = any>(url: string, config?: AxiosRequestConfig): Promise<void|AxiosResponse<any>> {
        return this.client
            .get(url, this.addAuthorization(config))
            .catch((error: any) => this.exceptionHandler(error))
    }

    protected post<T = any>(url: string, data?: any, config?: AxiosRequestConfig): Promise<void|AxiosResponse<any>> {
        return this.client
            .post(url, data, this.addAuthorization(config))
            .catch((error: any) => this.exceptionHandler(error))
    }

    protected delete<T = any>(url: string, config?: AxiosRequestConfig): Promise<void|AxiosResponse<any>> {
        return this.client
            .delete(url, this.addAuthorization(config))
            .catch((error: any) => this.exceptionHandler(error))
    }

    protected exceptionHandler(error: any) {
        if (error.response && error.response.status && error.response.status === 417 && error.response.data.Message) {
            // validation error
            this.store.commit('showSnackbar', { message: error.response.data.Message, color: 'error', timeout: 10000 })
            throw new ValidationError(error.response.data.Message)
        } else if (error.response && error.response.status && error.response.status === 400 && error.response.data.Message) {
            // server error
            this.store.commit('showSnackbar', { message: error.response.data.Message, color: 'error', timeout: 10000 })
            throw new ServerError(error.response.data.Message)
        } else if (error.response && error.response.status && error.response.status === 401) {
            // invalid credentials
            throw new UnauthorizedRequestError((error.response || {data: { error: 'Unauthorize'}}).data.error)
        } else if (error.response && error.response.status && error.response.data.Message) {
            // tslint:disable-next-line:no-console
            this.store.commit('showSnackbar', { message: error.toString(), color: 'error', timeout: 10000 })
            throw new ServerException(error.response.data.Message)
        } else {
            // tslint:disable-next-line:no-console
            this.store.commit('showSnackbar', { message: error.toString(), color: 'error', timeout: 10000 })
            throw new NetworkError(error.message)
        }
    }

    protected convertDateToYMD( mdy?: string ): string|null {
        if (mdy === undefined || mdy === '') {
            return null;
        }
        const p = mdy.split('/');
        return p[2] + '-' + p[0] + '-' + p[1];
    }

    private addAuthorization(config?: AxiosRequestConfig): AxiosRequestConfig {
        if (typeof config === 'undefined') {
            config = {}
        }
        if (typeof config.headers === 'undefined') {
            config.headers = {}
        }
        // config.headers.Authorization = 'Bearer ' + this.token()
        return config
    }

    // private token(): string|null {
    //     const result = (this.store.state.auth.Ticket) ? this.store.state.auth.Ticket : localStorage.getItem('authTicket')
    //     if (result) {
    //         return result;
    //     } else {
    //         try {
    //             // @ts-ignore
    //             return NativeNotification.GetAuthTicket()
    //         } catch (e) {
    //             return null
    //         }
    //     }
    // }

}

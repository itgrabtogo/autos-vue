import cloneDeep from 'lodash/cloneDeep'

export enum WeekDays {
    Monday = 1,
    Tuesday = 2,
    Wednesday = 3,
    Thursday = 4,
    Friday = 5,
    Saturday = 6,
    Sunday = 7
}

export class Tools {
    static colorNames: any = {
        aqua: '#00ffff',
        blue: '#0000ff',
        brown: '#a52a2a',
        cyan: '#00ffff',
        darkblue: '#00008b',
        darkcyan: '#008b8b',
        darkgrey: '#a9a9a9',
        darkgreen: '#006400',
        darkkhaki: '#bdb76b',
        darkmagenta: '#8b008b',
        darkolivegreen: '#556b2f',
        darkorange: '#ff8c00',
        darkorchid: '#9932cc',
        darkred: '#8b0000',
        darksalmon: '#e9967a',
        darkviolet: '#9400d3',
        fuchsia: '#ff00ff',
        gold: '#ffd700',
        green: '#008000',
        indigo: '#4b0082',
        khaki: '#f0e68c',
        lightblue: '#add8e6',
        lightcyan: '#e0ffff',
        lightgreen: '#90ee90',
        lightgrey: '#d3d3d3',
        lightpink: '#ffb6c1',
        lightyellow: '#ffffe0',
        lime: '#00ff00',
        magenta: '#ff00ff',
        maroon: '#800000',
        navy: '#000080',
        olive: '#808000',
        orange: '#ffa500',
        pink: '#ffc0cb',
        violet: '#800080',
        red: '#ff0000',
        silver: '#c0c0c0'
    };

    static getTargetElementIndex(formElements: any, targetElement: any) {
        const formLength = formElements.length
        for (let i = 0; i < formLength; i++) {
          if (formElements[i] === targetElement) {
            return i;
          }
        }
        return -1
    }

    static focusNextInputField(event: any) {
        const formElements = [...event.target.form]
        const formLength = formElements.length
        for (let ti = 0; ti < formLength; ti++) {
            const el = formElements[ ti ]
            if (!el.dataset.tabindex) {
                el.dataset.tabindex = el.tabIndex || ti + 1;
            }
        }
        formElements.sort( (n1, n2) => n1.dataset.tabindex - n2.dataset.tabindex )
        const ndx = Tools.getTargetElementIndex(formElements, event.target)
        if (ndx + 1 < formLength) {
            let nextEl = formElements[ndx + 1];
            if (!nextEl) {
                return false
            }
            if (nextEl.type === 'button') {
                if (ndx + 2 < formLength) {
                    nextEl = formElements[ndx + 1 + 1];
                } else {
                    return false
                }
            }
            nextEl.focus()
            return true
        } else {
            return false
        }
    }

    static clone(o: any): any {
      return cloneDeep(o)
    }

    static isEmptyDate(d?: Date): boolean {
        return !( d && new Date(d).getFullYear() > 2000 )
    }

    static isEmptyString(str?: string): boolean {
        return !( str && str.trim() !== '')
    }

    static isValidDate(d: any): boolean {
        return Tools.isValidDate(d)
    }

    static isValidDateString(d: any): boolean {
        return Tools.isValidDateString(d)
    }

    static getTimePassed(created: Date): string {
        const now = new Date()
        const totalMinutes = ( (now.getTime() - created.getTime() ) / 1000 )

        const min = Math.floor(totalMinutes / 60)
        const sec = Math.floor(totalMinutes % 60)

        return min + ':' + ( (sec < 10) ? '0' : '' ) + sec
    }

    static createHeaderArray(n: number) {
        const result = []
        for (let i = 0; i < n; i++) {
            result.push( { value: 'id', text: 'Name' } );
        }
        return result
    }

    static getWeekDay(n: number): string {
        if (n === 0) {
            return WeekDays[7]
        }
        return WeekDays[n]
    }

    static getToday(): Date {
        const d = new Date()
        d.setHours(0, 0, 0, 0)
        return d
    }

    static getCurrentTime(addHour = 0): string {
        let d = new Date()
        if (addHour > 0) {
            d = new Date(d.setHours(d.getHours() + addHour))
        }
        const h = this.formatTime(d.getHours())
        const m = this.formatTime(d.getMinutes())

        return h + ':' + m
    }

    static formatTime(i: number): string {
        if (i < 10) {
            return '0' + i
        }
        return i.toString()
    }

    static getTimeAsIntForComparision(time: string) {
        return parseInt(time.substring(0, 2) + time.substring(3, 5), 10)
    }

    static toISODateFormat(date: string) {
        // YYYY-MM-DD
        if (Tools.isEmptyString(date)) {
            return ''
        }
        const d = new Date(Date.parse(date))
        return d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate()
    }
}

// tslint:disable:max-classes-per-file

export class ValidationError {
    message: string

    constructor(msg: string) {
        this.message = msg
    }

    toString() {
        return this.message
    }
}

export class ServerError {
    message: string

    constructor(msg: string) {
        this.message = msg
    }

    toString() {
        return this.message
    }
}

export class NetworkError {
    message: string

    constructor(msg: string) {
        this.message = msg
    }

    toString() {
        return this.message
    }
}

export class UnauthorizedRequestError {
    message: string

    constructor(msg: string) {
        this.message = msg
    }

    toString() {
        return this.message
    }
}

export class ServerException {
    message: string

    constructor(msg: string) {
        this.message = msg
    }

    toString() {
        return this.message
    }
}

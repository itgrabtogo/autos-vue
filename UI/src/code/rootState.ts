import { AuthState } from './authState'

interface RootState {
  auth: AuthState
}

export {
  RootState
}

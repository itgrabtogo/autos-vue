import {Module, MutationTree, ActionTree, GetterTree} from 'vuex'
import {AxiosResponse} from 'axios'
import {RootState} from './rootState'
import {API} from './api'
import {UnauthorizedRequestError} from './errors'

class AuthAPI extends API {
  async authenticate(data: { userName: string, password: string, androidDeviceToken: string }) {
    try {
      const response = await this.client.post('User/Authenticate', data)
      return response.data
    } catch (error) {
      this.exceptionHandler(error)
    }
  }
  async logout() {
    const response = await this.get('User/Logout')
    return (response as AxiosResponse<any>).data
  }
}

interface AuthState {
  FullName: string | null
  UserName: string | null
  Ticket: string | null
  Expires: Date
}

const mutations: MutationTree<AuthState> = {
  login(s, loginInfo: { FullName: string, UserName: string, Ticket: string, Expires: Date ) {
    // s.FullName = loginInfo.FullName
    // s.UserName = loginInfo.UserName
    // s.Ticket = loginInfo.Ticket
    // s.Expires = new Date( loginInfo.Expires )
    // localStorage.setItem('authTicket', s.Ticket)
    // localStorage.setItem('authFullName', s.FullName)
    // localStorage.setItem('authUserName', s.UserName)
    // localStorage.setItem('authExpires', s.Expires.getTime().toString())
    // try {
    //   // @ts-ignore
    //   NativeNotification.SetAuthTicket(s.Ticket)
    // } catch (e) {
    //   // ignore
    // }
  },
  logout(s) {
    // s.FullName = null
    // s.UserName = null
    // s.Ticket = null
    // localStorage.removeItem('authTicket')
    // localStorage.removeItem('authFullName')
    // localStorage.removeItem('authUserName')
    // localStorage.removeItem('authExpires')
    // try {
    //   // @ts-ignore
    //   NativeNotification.SetAuthTicket('')
    // } catch (e) {
    //   // ignore
    // }
  }
}

const actions: ActionTree<AuthState, RootState> = {
  async login({ commit, rootState }, data: { userName: string, password: string, androidDeviceToken: string }) {
    const api = new AuthAPI({ commit, state: rootState })
    const loginInfo = await api.authenticate(data)
      .catch((error: any) => {
        if (error instanceof UnauthorizedRequestError) {
          throw error
        } else {
          commit('showSnackbar', { message: error.toString(), color: 'error', timeout: 10000 })
        }
      })
    if (!loginInfo) {
      commit('logout')
      return false
    }
    commit('login', loginInfo)
    return loginInfo
  },
  async logout({ commit, rootState }) {
    const api = new AuthAPI({ commit, state: rootState })
    api.logout()
    commit('logout')
  },
}

const getters: GetterTree<AuthState, RootState> = {
  loggedIn(s) {
    if (s.Ticket === null) {
      return false
    }
    if (typeof s.Ticket === 'undefined') {
      return false
    }
    return ( s.Ticket.length && s.Ticket.length > 10 )
  },
  authTicketExpiration(s) {
    if (s.Ticket === null) {
      return new Date(1900, 1, 1)
    }
    if (typeof s.Ticket === 'undefined') {
      return new Date(1900, 1, 1)
    }
    return s.Expires
  },
  userName(s) {
    if (s.Ticket === null) {
      return ''
    }
    if (typeof s.Ticket === 'undefined') {
      return ''
    }
    return s.FullName
  }
}

const state: AuthState = {
  UserName: localStorage.getItem('authUserName'),
  FullName: localStorage.getItem('authFullName'),
  Ticket: localStorage.getItem('authTicket'),
  Expires: localStorage.getItem('authExpires') ? new Date( parseInt( localStorage.getItem( 'authExpires' )!, 10 ) ) : new Date()
}

const auth: Module<AuthState, RootState> = {
  mutations,
  actions,
  getters,
  state
}

export {
  auth,
  AuthState
}

import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { sync } from 'vuex-router-sync'
import vuetify from './plugins/vuetify'

import 'material-design-icons-iconfont/dist/material-design-icons.css'
import '@fortawesome/fontawesome-pro/css/all.min.css'
import './styles/index.styl'

Vue.config.productionTip = false;

// tslint:disable-next-line:no-var-requires
const VuetifyConfirm = require('vuetify-confirm')
// tslint:disable-next-line:no-var-requires
const GlobalEvents = require('vue-global-events')

Vue.use(GlobalEvents)
Vue.component('global-events', GlobalEvents)
Vue.use(VuetifyConfirm, { vuetify })

sync(store, router)

new Vue({
  router,
  store,
  // @ts-ignore
  vuetify,
  render: (h) => h(App)
}).$mount('#app');

import Vue from 'vue'
import Router from 'vue-router'
import store from '@/store'

import Auctions from '@/views/Auctions.vue'

Vue.use(Router)

const router = new Router({
  // const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      redirect: { name: 'auctions' },
    },
    {
      path: '/auctions/',
      name: 'auctions',
      component: Auctions,
      meta: { title: 'Auctions' }
    },
  ],
})

export default router

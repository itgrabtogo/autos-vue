import Vue from 'vue'

declare module 'vue/types/vue' {
    interface Vue {
        $confirm: (message: string) => boolean
    }
}

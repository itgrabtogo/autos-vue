import {Module, MutationTree, ActionTree} from 'vuex'
import {RootState} from '@/code/rootState'

interface SnackbarState {
  message: string | null,
  color: string,
  timeout: number,
  rnd: Date
}

const mutations: MutationTree<SnackbarState> = {
    showSnackbar(s, info: { message: string, color?: string, timeout?: number }) {
      s.message = info.message
      s.color = info.color || 'primary'
      s.timeout = info.timeout || 4000
      s.rnd = new Date()
    }
}

const Snackbar: Module<SnackbarState, RootState> = {
  mutations,
  state: {
    message: '',
    color: 'primary',
    timeout: 4000,
    rnd: new Date()
  }
}

export {
  Snackbar,
  SnackbarState
}

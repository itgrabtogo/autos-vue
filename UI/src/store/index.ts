import Vue from 'vue'
import Vuex, {StoreOptions} from 'vuex'
import {RootState} from '@/code/rootState'

import {Snackbar} from './snackbar'

const debug = process.env.NODE_ENV !== 'production'

Vue.use(Vuex)

const store: StoreOptions<RootState> = {
  strict: debug,
  modules: {
    Snackbar,
  }
}

export default new Vuex.Store<RootState>(store)
